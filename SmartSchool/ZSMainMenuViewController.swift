//
//  ZSMainMenuViewController.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ZSMainMenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var selectStudentButton: UIButton!
    @IBOutlet weak var pickerHolderView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!

    var selectedStudentIndex = Int()
    var studentData = [ZSStudentObject]()
    var isPickerViewOpened: Bool = false
    var fireBaseToken: String = ""

    var collectionTitles: [String] = ["متابعة الباص", "تغيير النقطة المفعلة", "إضافة نقطة جديدة", "تغيير كلمة السر"]

    var collectionImages: [String] = ["trackBusIcon", "changeActivePointIcon", "addnewPointIcon","changePasswordIcon"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(studentData)
        
        self.title = "Smart School"
        var image = UIImage(named: "logoutButtonIcon")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.logout))
        self.navigationController?.navigationBar.barTintColor = Colors.colorFromHex(0xfaa424)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

        self.collectionView.delegate   = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ZSMainMenuCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ZSMainMenuCollectionViewCell")
        
        self.selectStudentButton.setTitle("\(studentData[0].text)", for: UIControlState.normal)
        
        self.connectAPICall()
    }
    
    func connectAPICall() {
        
        let token: String    = UserDefaults.standard.string(forKey: "token")!
        let userID: String   = UserDefaults.standard.string(forKey: "userID")!
        var fireBaseToken: String? = String()
        
        if UserDefaults.standard.string(forKey: "fireBaseToken") != nil {
             fireBaseToken = UserDefaults.standard.string(forKey: "fireBaseToken")!
            self.fireBaseToken = fireBaseToken!
//            self.showTokenView(stringToken: fireBaseToken!)
        } else {
            let alert = UIAlertController(title: nil, message: "NIL TOKEN", preferredStyle: UIAlertControllerStyle.alert)
            
            let dismiss = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action) in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(dismiss)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        let deviceUUID = UIDevice.current.identifierForVendor!.uuidString
        let deviceLanguage = Locale.current.languageCode ?? ""
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? ""
        let deviceModel = UIDevice.current.model
        let deviceSystemVersion = UIDevice.current.systemVersion
        let deviceSystemName    = UIDevice.current.systemName
        let appCodeBuildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String 
        //let Zahl = (appCodeBuildNumber as NSString).floatValue
        //let twoDecimalPlaces = String(format: "%.2f", Zahl)
        let num: Int = Int(appCodeBuildNumber)!
        
        print("App Code Build Number : \(num)")
        //
        print("Device UUID : \(deviceUUID)")
        //8D700266-3FF9-4C12-818D-8793A8F7CEA5
        print("Device APP Version :\(appVersion)")
        //1.0
        print("Device language Code : \(deviceLanguage)")
        //("en")-("US")
        print("Device Model : \(deviceModel)")
        //iPhone
        print("Device Description : \(UIDevice.current.description)")
        //<UIDevice: 0x608000022420>
        print("Device Localized Model : \(UIDevice.current.localizedModel)")
        //iPhone
        print("Device Name : \(UIDevice.current.name)")
        //Mohyee’s MacBook Pro
        print("Device System Version : \(deviceSystemVersion)")
        //10.2
        print("Device System : \(UIDevice.current.systemName)")
        //iOS
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]
        
        let params = ["user_id":            userID,
                      "device_type":        deviceModel,            //iPhone
                      "device_token":       fireBaseToken!,
                      "device_uuid":        deviceUUID,             //UDID
                      "app_code":           num,     //Build
                      "app_version":        appVersion,             //1.0
                      "os_version":         deviceSystemVersion,    //10.2
                      "device_language":    deviceLanguage,         //en
                      "device_model":       deviceModel,            //iPhone 6s
                      "longitude":          "",
                      "latitude":           "",
                      "device_manufacture": "Apple",                //Apple
                      "os_name":            deviceSystemName,       //iOS
                      "ip":                 "",]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_CONNECT, parameters: params, headers: headers) { (data) in
            if data != nil {
                self.loading.stopAnimating()
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData)
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }

    }
    
    // MARK: Select Student Methods and Delegates
    
    @IBAction func selectStudentAction(_ sender: UIButton) {
        print("Select Student Action")
        
        self.pickerView.delegate   = self
        self.pickerView.dataSource = self
        
        if self.isPickerViewOpened {
            self.isPickerViewOpened = false
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = -108
            })
        } else {
            self.isPickerViewOpened = true
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = 64
            })
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studentData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(studentData[row].text)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedStudentIndex = row
        self.selectStudentButton.setTitle("\(studentData[row].text)", for: UIControlState.normal)
    }
    
    func logout(){
        
        let alert = UIAlertController(title: nil , message: "هل تريد الخروج ؟", preferredStyle: UIAlertControllerStyle.alert)
        
        let option1 = UIAlertAction(title: "موافق", style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            
            KeychainWrapper.standard.removeObject(forKey: "userName")
            KeychainWrapper.standard.removeObject(forKey: "password")
            
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(option1)
        
        let option2 = UIAlertAction(title: "إالغاء", style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(option2)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZSMainMenuCollectionViewCell", for: indexPath) as? ZSMainMenuCollectionViewCell
        
        cell?.nameLabel.text = collectionTitles[indexPath.row]
        cell?.imageView.image = UIImage(named: collectionImages[indexPath.row])
        
        cell?.backGroundView.layer.cornerRadius = 5.0
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: Constants.SEGUE_TRACK_BUS, sender: self)
        } else if indexPath.row == 1 {
            self.performSegue(withIdentifier: Constants.SEGUE_CHANGE_ACTIVE_POINT, sender: self)
        } else if indexPath.row == 2 {
            self.performSegue(withIdentifier: Constants.SEGUE_SET_ACTIVE, sender: self)
        } else if indexPath.row == 3 {
            self.performSegue(withIdentifier: Constants.SEGUE_CHANGE_PASSWORD, sender: self)
        }
 
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.SEGUE_CHANGE_ACTIVE_POINT {
            let changeActivePointController = segue.destination as! ZSChangeActivePointsViewController
            changeActivePointController.selectedStudentIndex = self.selectedStudentIndex
        } else if segue.identifier == Constants.SEGUE_SET_ACTIVE {
            let addPointController = segue.destination as! ZSSetActivePointViewController
            addPointController.studentData = self.studentData
            addPointController.selectedStudentIndex = self.selectedStudentIndex
        }
 
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    func showTokenView(stringToken: String){
        var alert = UIAlertController(title: "Duplicate file", message: stringToken, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Ok button")
            //print(self.textField.text)
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func configurationTextField(textField: UITextField!){
        textField.text = self.fireBaseToken
    }
    

}
