//
//  ZSLoginViewController.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/3/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class ZSLoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    
    var studentData = [ZSStudentObject]()
    var savedUserName : Bool = Bool()
    var savedPassword : Bool = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.userNameTextField.tag = -10
        self.passwordTextField.tag = -20
        
        self.userNameTextField.textColor = UIColor.white
        
        let retrievedString: String? = KeychainWrapper.standard.string(forKey: "userName")

        if retrievedString != nil {
            self.userNameTextField.text = KeychainWrapper.standard.string(forKey: "userName")
            self.passwordTextField.text = KeychainWrapper.standard.string(forKey: "password")
            
            self.enterAction(self.enterButton)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == -10 {
            self.userNameTextField.text = ""
            self.userNameTextField.textColor = UIColor.white
        }
        if textField.tag == -20 {
            self.passwordTextField.text = ""
        }
    }
    
    @IBAction func saveUserNameAndPassword(_ sender: UISwitch) {
        
    }

    @IBAction func enterAction(_ sender: UIButton) {

        if self.userNameTextField.text == "" || self.userNameTextField.text == "User name is required" {
            self.userNameTextField.text = "User name is required"
            self.userNameTextField.textColor = UIColor.red
            return
        }
        
//        let numberFormatter = NumberFormatter()
//        numberFormatter.locale = Locale(identifier: "EN")
//        
//        if let userNameNumber = numberFormatter.number(from: self.userNameTextField.text!)
//        {
//            print("UserName is: ", userNameNumber)
//            self.userNameTextField.text = "0" + userNameNumber.stringValue
//        }
        
        self.userNameTextField.text = Util.replaceArabicNumberWithEnglishFrom(number: self.userNameTextField.text!)
        
        self.passwordTextField.text = Util.replaceArabicNumberWithEnglishFrom(number: self.passwordTextField.text!)

        
//        if let passWordNumber = numberFormatter.number(from: self.passwordTextField.text!)
//        {
//            print("Password is: ", passWordNumber)
//            self.passwordTextField.text = passWordNumber.stringValue
//        }

        self.loading.isHidden = false
        self.loading.startAnimating()
        
        let params = ["login": self.userNameTextField.text!,
                      "password": self.passwordTextField.text!]

        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_LOGIN, parameters: params, headers: nil) { (data) in
            
            if data != nil {
                self.loading.stopAnimating()
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    
                    if self.rememberMeSwitch.isOn {
                        if !self.savedUserName  {
                            let userName: String = self.userNameTextField.text!
                            let password: String = self.passwordTextField.text!
                            
                            self.savedUserName = KeychainWrapper.standard.set(userName, forKey: "userName")
                            self.savedPassword = KeychainWrapper.standard.set(password, forKey: "password")
                        }
                    }
                    
                    self.userNameTextField.text = ""
                    self.passwordTextField.text = ""
                    
                    let userToken = jsonData["token"].stringValue
                    
                    print(jsonData["message"])
                    let parentObject = ZSParentObject(parent: jsonData["parent"])
                    print(parentObject.middle_name)
                    
                    let userObject = ZSUserObject(user: jsonData["user"])
                    print(userObject.phone_number)
                    print(userObject.created_at)
                    print(userObject.hashedPassword)
                    print(userObject.type)
                    
                    if userObject.type != "1" {
                        let refreshAlert = UIAlertController(title: nil, message: "يرجى الدخول بمستخدم لاولياء الامور", preferredStyle: .alert)
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(refreshAlert, animated: true, completion: nil)
                        return
                    }
                    
                    UserDefaults.standard.set(userObject._id, forKey: "userID")
                    UserDefaults.standard.set(parentObject.id, forKey: "parentID")
                    UserDefaults.standard.set(userToken, forKey: "token") //setObklject

                    self.studentData = [ZSStudentObject]()

                    for temp in jsonData["students"].arrayValue {
                        let studentObject = ZSStudentObject(student: temp)
                        self.studentData.append(studentObject)
                        
                        
                        print("Student Info : \n First Name :  \(studentObject.first_name) | National ID :  \(studentObject.national_id) | Educational Documents :  \(studentObject.classroom_id)")
                        
                    }
                    
                    self.performSegue(withIdentifier: "loginSegue", sender: self)
                    
                } else if !jsonData["success"].boolValue {
                    self.loading.stopAnimating()
                    
                    var messageAlert = String()
                    
                    if jsonData["message"].string == "User not found" {
                        messageAlert = "اسم المستخدم غير متوفر"
                    } else {
                        messageAlert = "خطأ في كلمة المرور"
                    }
                    
                    let refreshAlert = UIAlertController(title: nil, message: messageAlert, preferredStyle: .alert)
                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(refreshAlert, animated: true, completion: nil)
                    
//                    KVNProgress.showError(withStatus: jsonData["message"].stringValue)
                }

            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
        
//        
//        Alamofire.request(Constants.URL_LOGIN, method: .post, parameters: params, encoding: JSONEncoding.default)
//            .responseJSON { response in
//                
//                if let json = response.result.value {
//                    print("JSON: \(json)")
//                    
//                    let jsonData = JSON(data: response.data!)
//
//                    if jsonData["status"].stringValue == "success" {
//                        self.loading.startAnimating()
//
//                        let userToken = jsonData["token"].stringValue
//                        UserDefaults.standard.set(userToken, forKey: "token") //setObject
//                        
//                        print(jsonData["message"])
//                        let parentObject = ZSParentObject(parent: jsonData["parent"])
//                        print(parentObject.middle_name)
//                        UserDefaults.standard.set(parentObject.id, forKey: "parentID")
//
//                        let userObject = ZSUserObject(user: jsonData["user"])
//                        print(userObject.phone_number)
//                        print(userObject.created_at)
//                        print(userObject.hashedPassword)
//                        print(userObject.type)
//                        UserDefaults.standard.set(userObject._id, forKey: "userID")
//                        
//                        for temp in jsonData["students"].arrayValue {
//                            let studentObject = ZSStudentObject(student: temp)
//                            self.studentData.append(studentObject)
//                            
//                            print("Student Info : \n First Name :  \(studentObject.first_name) | National ID :  \(studentObject.national_id) | Educational Documents :  \(studentObject.classroom_id)")
//
//                        }
//                        self.performSegue(withIdentifier: "loginSegue", sender: self)
//                    } else if !jsonData["success"].boolValue {
//                        self.loading.stopAnimating()
//                        KVNProgress.showError(withStatus: jsonData["message"].stringValue)
//                    }
//                } else {
//                    self.loading.stopAnimating()
//                    print("Did not receive json")
//                }
//        }

        //        let manager = AFHTTPSessionManager()
        //        manager.requestSerializer  = AFJSONRequestSerializer()
        //        manager.responseSerializer = AFHTTPResponseSerializer()
        //
        //        manager.post(urlString, parameters: dictionary, success: { (success, data) in
        //
        //            let json = JSON(data: data as! Data)
        //            if json["status"].stringValue == "success" {
        //                print(json["message"])
        //            }
        //
        //        }) { (failure, error) in
        //            print (error.localizedDescription)
        //        }
    }

    
     // MARK: - Navigation
    
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "loginSegue" {
            let navigationView = segue.destination as! UINavigationController
            let mainView = navigationView.topViewController as! ZSMainMenuViewController
            mainView.studentData = self.studentData
     }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
