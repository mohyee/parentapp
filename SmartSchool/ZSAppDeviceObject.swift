//
//  ZSAppDeviceObject.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/11/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSAppDeviceObject: NSObject {
    
    var app_code           = Int()
    var app_version        = String()
    var device_language    = String()
    var device_manufacture = String()
    var device_model       = String()
    var device_token       = String()
    var device_type        = String()
    var device_uuid        = String()
    var ip                 = String()
    var latitude           = String()
    var longitude          = String()
    var os_name            = String()
    var os_version         = String()

    override init() { }
    
    init(appDevice:JSON) {
        super.init()
        
        app_code              = appDevice["app_code"].intValue
        app_version           = appDevice["app_version"].stringValue
        device_language       = appDevice["device_language"].stringValue
        device_manufacture    = appDevice["device_manufacture"].stringValue
        device_model          = appDevice["device_model"].stringValue
        device_token          = appDevice["device_token"].stringValue
        device_type           = appDevice["device_type"].stringValue
        device_uuid           = appDevice["device_uuid"].stringValue
        ip                    = appDevice["ip"].stringValue
        latitude              = appDevice["latitude"].stringValue
        longitude             = appDevice["longitude"].stringValue
        os_name               = appDevice["os_name"].stringValue
        os_version            = appDevice["os_version"].stringValue
    }
}


//        "app_device": {
//            "app_code": 1,
//            "app_version": "1.0",
//            "device_language": "ar",
//            "device_manufacture": "samsung",
//            "device_model": "SM-N920C",
//            "device_token": "ewLaUDT9ahA:APA91bEYn0JVBGKYA2wc1gOuIY3n3VhkeW6FFqIQ8SVGibnhLBDFy74UgWBAvtzDEeV7ch_ePJYjuRpbIj2mQt2_P1gi2FtiIg-TQ_xDYtya42rrGY_rVECBZujHSSDH2dhRtAKEZiQ7",
//            "device_type": "Android",
//            "device_uuid": "a450488f-67d3-457a-8db5-1e08932eec4b",
//            "ip": "10.164.123.169",
//            "latitude": "0.0",
//            "longitude": "0.0",
//            "os_name": "android",
//            "os_version": "6.0.1"
//        },
