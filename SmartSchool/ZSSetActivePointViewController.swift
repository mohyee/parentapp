//
//  ZSSetActivePointViewController.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/25/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import CoreLocation

class ZSSetActivePointViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var selectStudentButton: UIButton!

    @IBOutlet weak var pickerHolderView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    lazy var geocoder = GMSGeocoder()
    var userLocation: CLLocation?
    
    var manager = CLLocationManager()
    var marker = GMSMarker()
    var selectedAddress: GMSAddress!
    var selectedType = Int()
    var isPickerViewOpened: Bool = false

    var studentData = [ZSStudentObject]()
    var selectedStudentIndex = Int()
    var pointName = String()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.title = "أضافة نقطة جديدة"
        
        let doneButton   = UIBarButtonItem(title: "إضافة",  style: .plain, target: self, action: #selector(self.askForConfirmation))
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white

        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        marker.icon = UIImage(named: "storesMapNeasrestPin.png")
        mapView.mapType = kGMSTypeHybrid
        mapView.isMyLocationEnabled = true
        mapView.isUserInteractionEnabled = true
        mapView.settings.consumesGesturesInView = false

        self.view.isUserInteractionEnabled = true
        
        let camera = GMSCameraPosition.camera(withLatitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude, zoom: 17)
        mapView.camera = camera
        
        //marker.position = camera.target
        //marker.map = mapView
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLocation(gesture:)))
//        mapView.addGestureRecognizer(tap)
        
        self.selectStudentButton.setTitle("\(self.studentData[self.selectedStudentIndex].text)", for: UIControlState.normal)
        self.selectedType = 2
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(touches)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location:CLLocation = locations[0]
        
        let latitude:CLLocationDegrees  = location.coordinate.latitude
        let longitude:CLLocationDegrees = location.coordinate.longitude
        
        // The myLocation attribute of the mapView may be null
        if userLocation == nil {
            userLocation = mapView.myLocation
            print("User's location: \(userLocation)")
            
            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 18)
            self.mapView.camera = camera
            
//            self.geocoder.reverseGeocodeCoordinate((location.coordinate)) { (placemarks, error) in
//                self.processResponse(withPlacemarks: placemarks!, error: error)
//            }
//            
//            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
//            self.mapView.camera = camera
        } else {
            print("User's location is unknown")
        }
    }
    
    func tapLocation(gesture: UITapGestureRecognizer) {
        
        let position = gesture.location(in: self.view)
        let coordinates:CLLocationCoordinate2D = mapView.projection.coordinate(for: position)
        
        marker.position = coordinates
        marker.map = mapView
        self.geocoder.reverseGeocodeCoordinate(coordinates) { (placemarks, error) in
            print("Done \(placemarks!)")
            if placemarks != nil {
                self.processResponse(withPlacemarks: placemarks!, error: error)
            }
        }
    }
    
    private func processResponse(withPlacemarks placemarks:GMSReverseGeocodeResponse, error: Error?) {
        
        self.selectedAddress = placemarks.firstResult()!
        print("Address \(selectedAddress)")
        
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
//            self.addressLabel.text = "Unable to Find Address for Location".localized(self.currentLanguage)
        } else {
            if selectedAddress.lines != nil {
                if selectedAddress.lines!.count > 0 {
                    let street = selectedAddress.lines![0]
                    let locality = selectedAddress.locality == nil ? "" : selectedAddress.locality!
                    let country = selectedAddress.country == nil ? "" : selectedAddress.country!
                    print("\(street), \(locality), \(country)")
//                    self.addressLabel.text = ("\(street), \(locality), \(country)")
                }
            }
        }
    }
    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            // type 2 drop حضور
            self.selectedType = 2
        } else if sender.selectedSegmentIndex == 1 {
            // type 1 pull مغادرة
            self.selectedType = 1
        } else if sender.selectedSegmentIndex == 2 {
            // type 3 drop & pull حضور ومغادرة
            self.selectedType = 3
        }
    }
 
    func askForConfirmation() {
        
        var pointType = String()
        
        if self.selectedType == 2 {
            pointType = "حضور"
        } else if self.selectedType == 1 {
            pointType = "مغادرة"
        } else if self.selectedType == 3 {
            pointType = "حضور ومغادرة"
        }
        
        let studentName  = self.selectStudentButton.currentTitle!
        let message = "اضغط موافق لإضافة نقطة \(pointType) للطالب \(studentName)"
        
        let alertController = UIAlertController(title: "تأكيد العملية", message: message, preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "موافق", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                // store your data
                
                if field.text == "" {
                    field.becomeFirstResponder()
                } else {
                    self.pointName = field.text!
                    self.setActivePointServerCall()
                }
            } else {
                // user did not fill field
                return
            }
        }
        
        let cancelAction = UIAlertAction(title: "الغاء", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "اسم النقطة"
            textField.textAlignment = .right
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    func setActivePointServerCall() {

        let token: String     = UserDefaults.standard.string(forKey: "token")!
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]

        let parameters = ["record": [
            "student_id": self.studentData[self.selectedStudentIndex]._id,
            "title": self.pointName,
            "type": ["id": "\(self.selectedType)"],
            "latitude": "\(mapView.camera.target.latitude)",
            "longitude": "\(mapView.camera.target.longitude)"
            ]] as [String : Any]

        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_ADD_POINT, parameters:  parameters,headers:headers) { (data) in
            print(data ?? "")
            
            if data != nil {
                self.loading.stopAnimating()
                let jsonData = JSON(data!)
                if jsonData["status"].stringValue == "success" {
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                        self.navigationController?.popViewController(animated: true)

                    })
                } else {
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                        self.navigationController?.popViewController(animated: true)
                    })
//                    KVNProgress.showError(withStatus: jsonData["message"].stringValue )
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
    }
    
    // MARK: Select Student Methods and Delegates
    
    @IBAction func selectStudentAction(_ sender: UIButton) {
        print("Select Student Action")
        
        self.pickerView.delegate   = self
        self.pickerView.dataSource = self
        
        if self.isPickerViewOpened {
            self.isPickerViewOpened = false
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = -108
            })
        } else {
            self.isPickerViewOpened = true
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = 64
            })
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studentData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(studentData[row].text)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedStudentIndex = row
        self.selectStudentButton.setTitle("\(studentData[row].text)", for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
