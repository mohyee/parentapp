//
//  Util.swift
//  FourPictures
//
//  Created by Amer Smadi on 9/3/14.
//  Copyright (c) 2014 Kindi. All rights reserved.
//

import UIKit

open class Util {
    
    static var appDelegate = UIApplication.shared.delegate as! AppDelegate
    public typealias ResultCallback = ((Bool) -> ())

    open static func iPadDevice() -> Bool {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return true
        }
        
        return false
    }
    
    open static func getSystemVersion() -> Float {
        return NSString(string: UIDevice.current.systemVersion).floatValue
    }
    
    open static func getScreenSize() -> CGSize {
        
        var screenWidth: CGFloat = UIScreen.main.bounds.size.width
        var screenHeight: CGFloat = UIScreen.main.bounds.size.height
        
        if Util.iPadDevice() {
            
            if UIApplication.shared.statusBarOrientation.isPortrait || UIDevice.current.orientation.isFlat {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth > screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
                
            } else if UIDevice.current.orientation.isLandscape {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth < screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
            } else {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth > screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
            }
            
        } else {
            
            if UIApplication.shared.statusBarOrientation.isPortrait || UIDevice.current.orientation.isFlat {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth > screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
                
            } else if UIApplication.shared.statusBarOrientation.isLandscape  {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth < screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
            } else {
                if Util.getSystemVersion() < 8.0 {
                    if screenWidth > screenHeight {
                        screenWidth = UIScreen.main.bounds.size.height
                        screenHeight = UIScreen.main.bounds.size.width
                    }
                }
            }
        }
        
        return CGSize(width: screenWidth, height: screenHeight)
    }
    
 

    
    open static func colorFromHex(color: String) -> UIColor {
        
        var rgbValues: UInt32 = 0
        let scanner = Scanner(string: color)
        
        scanner.scanLocation = 0
        scanner.scanHexInt32(&rgbValues)
        
        return UIColor(red: CGFloat((rgbValues & 0xff0000) >> 16)/255.0, green: CGFloat((rgbValues & 0x00ff00) >> 8)/255.0, blue: CGFloat(rgbValues & 0x0000ff)/255.0, alpha: 1.0)
    }

    open static func viewRoundedCorner(view: UIView, cornerRadius: CGFloat) {
        view.layer.cornerRadius  = cornerRadius
        view.layer.borderWidth   = 1.0
        view.layer.borderColor   = UIColor.clear.cgColor
    }
    
    open static func buttonRoundedCorner(btn: UIButton, cornerRadius: CGFloat) {
        btn.layer.cornerRadius  = cornerRadius
        btn.layer.borderWidth   = 1.0
        btn.layer.borderColor   = UIColor.clear.cgColor
    }
    
    open static func makeShadowForView(view: UIView) {
        view.layer.shadowColor   = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset  = CGSize.zero
        view.layer.shadowRadius  = 2
    }
 
   
    open static func showInfoAlert(msg: String, controller: UIViewController, callback: @escaping ResultCallback) {
        
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let dismiss = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            callback(true)
        }
        alert.addAction(dismiss)
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    open static func twoOptionsAlert(title: String?, msg: String?, option1Title: String, option2Title: String, controller: UIViewController, callback: @escaping ResultCallback) {
        
        let alert = UIAlertController(title: title == nil ? nil : title!, message: msg == nil ? nil : msg!, preferredStyle: UIAlertControllerStyle.alert)
        
        let option1 = UIAlertAction(title: option1Title, style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            callback(true)
        }
        alert.addAction(option1)
        
        let option2 = UIAlertAction(title: option2Title, style: UIAlertActionStyle.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            callback(false)
        }
        alert.addAction(option2)
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    open static func isSubPointActive(activeForObj: AnyObject?)-> Bool {
        
        let activeObject = activeForObj as? ZSActiveForObject
        
        var isActiveFor = false
        
//        if (activeObject == nil || activeObject?.start?.characters.count == 0 || activeObject?.end?.characters.count == 0 ) {
//            return false
//        }
        
        let currentDate = Date()
        let convertedDate = currentDate.timeIntervalSince1970
        let currentDateInMillis =  Int64(convertedDate*1000)
        
        
        let startDate: Int64! = Int64((activeObject?.start)!)
        let endDate:   Int64! = Int64((activeObject?.end)!)
        
        if (currentDateInMillis >= startDate! && currentDateInMillis <= endDate!) {
            isActiveFor = true
        }
        return isActiveFor
    }
    
    open static func replaceArabicNumberWithEnglishFrom(number: String) -> String {
        
        let count = number.characters.count
        var newNumberString = String()
        let numberNSString  = number as NSString
        
        for index in 0..<count  {
            let character = numberNSString.substring(with: NSMakeRange(index, 1))
            //            print("Character : \(character)")
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en")
            let number = formatter.number(from: character)
            if number == nil {
                newNumberString.append(character)
                continue
            }
            //            print("Number : \(number!)")
            newNumberString.append(number!.stringValue)
        }
        print("NEW NUMBER STRING : \(newNumberString)")
        
        return newNumberString
    }

}
