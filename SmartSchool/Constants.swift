//
//  Constants.swift
//  JarirBookstore
//
//  Created by Amer Smadi on 8/14/16.
//  Copyright © 2016 Amer Smadi. All rights reserved.
//

import Foundation

open class Constants {
    
    // MARK: GOOGLE MAPS
    
    static let GOOGLE_API_KEY: String = "AIzaSyB7XWATdkeLdZApdPqs7-3ywcUJ7cB5HYA"
    
    // MARK: SEGUES
    
    static let SEGUE_LOGIN: String               = "loginSegue"
    static let SEGUE_CHANGE_PASSWORD: String     = "changePasswordSegue"
    static let SEGUE_TRACK_BUS: String           = "trackBusSegue"
    static let SEGUE_CHANGE_ACTIVE_POINT: String = "changeActivePointsSegue"
    static let SEGUE_SET_ACTIVE: String          = "setActiveSegue"
    static let SEGUE_CALENDAR: String            = "calenderSegue"
    static let SEGUE_MAP: String                 = "mapSegue"

    // MARK: SERVICES
    
//    static let ZLIOUS_BASE_URL: String        = "http://146.185.169.237:3100/"
    static let ZLIOUS_BASE_URL: String        = "http://zlious.com/"
    
//    static let socketLink = "http://146.185.169.237:3100"
    static let socketLink = "http://zlious.com"

    static let URL_LOGIN: String              = ZLIOUS_BASE_URL + "login"
    static let URL_CONNECT: String            = ZLIOUS_BASE_URL + "api/users/connect"
    static let URL_GET_CHILDREN_TRIPS: String = ZLIOUS_BASE_URL + "api/parents/get-children-trips"
    static let URL_GET_ACTIVE_TRIPS: String   = ZLIOUS_BASE_URL + "api/parents/get-active-trips"
    static let URL_SET_ACTIVE_FOR: String     = ZLIOUS_BASE_URL + "api/points/set-activeFor"
    static let URL_CANCEL_ACTIVE_FOR: String  = ZLIOUS_BASE_URL + "api/points/cancel-activeFor"
    static let URL_ADD_POINT: String          = ZLIOUS_BASE_URL + "api/points/add"
    static let URL_CHANGE_PASSWORD: String    = ZLIOUS_BASE_URL + "login/change-password"
    
    // MARK: USER DEFAULTS KEYS
    
    static let DEVICE_TOKEN: String        = "GCM_TOKEN"
    static let CONTENT_TYPE: String        = "application/json"
    
    // MARK: SERVICES PARAMS
    
    static let PARAM_LOGIN: String           = "login"
    static let PARAM_PASSWORD: String        = "password"
    static let PARAM_ACCESS_TOKEN: String    = "x-access-token"
    static let PARAM_CONTENT_TYPE: String    = "Content-Type"
    static let PARAM_PARENT_ID: String       = "parent_id"
    static let PARAM_USER_ID: String         = "user_id"
    static let PARAM_POINT_ID: String        = "point_id"
    static let PARAM_SUB_POINT_ID: String    = "sub_point_id"
    static let PARAM_START: String           = "start"
    static let PARAM_END: String             = "end"


    //MARK: MSGS
    
    static let MSG_WORNG: String = "Something went wrong, check connection."
    let CONS_TEST: String = "Something went wrong, check connection."

}
