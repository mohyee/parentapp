//
//  ZSPointObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSGeneralPointObject: NSObject { //General Point Object
    
    var _id         = String()
    var __v         = String()
    var student_id  = [String]()
    var points :[ZSSubPointObject]?       = [ZSSubPointObject]()
    
    override init() { }
    
    init(generalPoint:JSON) {
        super.init()
        
        _id           = generalPoint["_id"].stringValue
        __v           = generalPoint["__v"].stringValue
//        student_id    = generalPoint["student_id"].arrayObject as! [String]

        for temp in generalPoint["points"].arrayValue {
            let subPointObject = ZSSubPointObject(subPoint: temp)
            points?.append(subPointObject)
        }
        
    }
}
