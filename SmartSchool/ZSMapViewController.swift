//
//  ZSMapViewController.swift
//  SmartSchool
//
//  Created by Mo7yee on 3/3/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import GoogleMaps

class ZSMapViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    
    var selectedSubPoint = ZSSubPointObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        
        self.title = "موقع النقطة"
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)

        let latitude  = Double(self.selectedSubPoint.latitude)
        let longitude = Double(self.selectedSubPoint.longitude)
        
        let locationCoordinate = CLLocationCoordinate2DMake(latitude!, longitude!)

        let marker   = GMSMarker(position: locationCoordinate)
        
        // type 1 pull مغادرة //green
        // type 2 drop حضور //Orange
        
        var pinImage = UIImage()
        
        if self.selectedSubPoint.isPrimary {
            if self.selectedSubPoint.type == 1 {
                pinImage = UIImage(named: "pinGreenPrimary.png")!
            } else {
                pinImage = UIImage(named: "pinOrangePrimary.png")!
            }
        } else {
            if self.selectedSubPoint.type == 1 {
                pinImage = UIImage(named: "pinGreen.png")!
            } else {
                pinImage = UIImage(named: "pinOrange.png")!
            }
        }
        
        marker.icon  = pinImage
        marker.title = self.selectedSubPoint.title
        
        marker.map = self.mapView
        let camera = GMSCameraPosition.camera(withTarget: locationCoordinate, zoom: 17)
        
        CATransaction.begin()
        CATransaction.setValue(Int(2), forKey: kCATransactionAnimationDuration)
        self.mapView.animate(to: camera)
        CATransaction.commit()
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func doneAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
