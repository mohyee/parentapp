//
//  ZSJournyObject.swift
//  SmartSchool
//
//  Created by Rawan Ala'eddin on 5/20/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

class ZSJournyObject: NSObject {
 
    
    
    var busId:String!
    var busNumber:String!
    var time:Int!
    var accuracy:Int!
    var speed:Int!
    var status:Int!
    var studentsInBus:Int!
    var totalStudents:Int!
    var location:CLLocationCoordinate2D? = nil
    
    
    init(_ object:JSON) {
        busId = object["busId"].stringValue
        busNumber = object["busNumber"].stringValue
        time = object["time"].intValue
        accuracy = object["accuracy"].intValue
        speed = object["speed"].intValue
        status = object["status"].intValue
        studentsInBus = object["studentsInBus"].intValue
        totalStudents = object["totalStudents"].intValue
        
        
        let latitude = object["latitude"].stringValue
        let longitude = object["longitude"].stringValue

        if latitude != "" && longitude != ""{
                location = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        }
        
 
    }

    
    
}
