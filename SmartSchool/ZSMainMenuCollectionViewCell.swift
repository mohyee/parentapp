//
//  ZSMainMenuCollectionViewCell.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit

class ZSMainMenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundView?.layer.cornerRadius = 5.0
    }

}
