 //
 //  SocketManager.swift
 //  demochat
 //
 //  Created by mit devices on 5/17/16.
 //  Copyright © 2016 mit devices. All rights reserved.
 //
 
 import Foundation
 import SwiftyJSON
 import UIKit
 import SocketIO
 import Firebase
 
 // MARK: Enums
 
 // MARK: Socket Manager Status
 
 /**
  Socket manager status.
  
  - NotConnected: Socket not connected.
  - Connected: Socket connected.
  
  */
 enum SocketManagerStatus: Int {
    
    case NotConnected
    case Connected
     
 }
 
 // MARK: Socket Manager Errors
 
 /**
  Socket manager errors.
  
  - NONE: No errors.
  ***
  - ConnectingError: Open connection.
  - DisconnectingError: Close connection.
  - AddingHandlerError: Adding handler to the socket.
  - RemovingHandlerError: Removing handler from socket.
  - SendingDataError: Sending data.
  ***
  - DataTypeUnknownError: Data type unknown received.
  ***
  - NoInternetConnectionError: No internet connection.
  - ConnectionTimedOutError: Connection timed out.
  ***
  - ConnectionReconnectAttemptsError: Connection reconnect attempts.
  - ConnectionTimeOutAfterError: Connection time out after.
  - ServerURLError: Server URL.
  
  */
 enum SocketManagerError {
    
    case NONE
    
    case ConnectingError
    case DisconnectingError
    case AddingHandlerError
    case RemovingHandlerError
    case SendingDataError
    
    case DataTypeUnknownError
    
    case NoInternetConnectionError
    case ConnectionTimedOutError
    
    case ConnectionReconnectAttemptsError
    case ConnectionTimeOutAfterError
    
    case ArrayIndexOutOfBoundsError
    
    case BuildingJsonPayloadError
    
    case dataNotReceived
    
 }
 
 // MARK: Message Data Types
 
 /**
  Message data types.
  
  - Unknown: Unknown data.
  - ServerCommand: Server command.
  ***
  - Typing: Indicator as this message is only typing.
  ***
  - Text: Data is text.
  - Image: Data is image.
  - Video: Data is video.
  - Voice: Data is voice.
  - Link: Data is link.
  - Location: Data is location.
  ***
  - ImageWithText: Data are image and text.
  - VideoWithText: Data are video and text.
  ***
  - Contact: Data is contact.
  
  */
 enum MessageDataType: Int {
    
    case unknown
    case serverCommand
    
    case typing
    
    case text
    case image
    case video
    case voice
    case link
    case location
    
    case imageWithText
    case videoWithText
    
    case contact
    
    func isMediaType() -> Bool{
        
        switch self {
            
        case .image:
            return true
        case .video:
            return true
        case .voice:
            return true
        case .location:
            return true
        case .imageWithText:
            return true
        case .videoWithText:
            return true
            
        default:
            return false
        }
    }
    
 }
 
 public enum EventsTypes {
    case busLocation
 }
 
 // MARK: Default Index For Payload
 
 /**
  Default Index For Payload.
  
  - MessageAck : 0 as Int
  - MessageID: 1 as Int.
  - MessageDataType: 2 as Int.
  - MessageData: 3 as Int.
  
  ***
  Don't change the order, only if you know what are you doing
  ***
  
  */
 enum DefaultIndexForPayload: Int {
    
    case MessageAck
    case MessageID
    case MessageDataType
    case MessageData
    
    
 }
 
 // MARK: Default Keys For Events
 
 /**
  Default Keys For Events.
  
  - Connected: connected as String.
  - Disconnected: disconnected as String.
  - ServerMessage: server_message as String.
  
  */
 enum EventsKeys: String {
    case Connected = "connect"
    case Disconnected = "disconnect"
    case Reconnect = "reconnect"
    case ReconnectAttempt = "reconnectAttempt"
    case Error = "error"
    case busLocation = "bus-location"
    case joinRoom = "join-room"
 }
 
 
 
 // MARK: Socket Manager Delegate
 
 protocol SocketManagerDelegate {
    
    
    /**
     Socket Manager Did Connect successfully Delegate.
     **/
    
    func socketManagerDidConnect()
    
    
    /**
     Socket Manager Did Disconnect successfully Delegate.
     **/
    
    func socketManagerDidDisconnect()
    
    /**
     Socket Manager Did Receive Error Delegate.
     
     Delegate to send socket manager error type.
     
     */
    func socketManagerDidReceiveError(error: SocketManagerError)
    
    func socketManagerDidReceive(error: SocketManagerError, forEvent event: EventsKeys)
    
    /**
     Socket Manager Did Receive Data Delegate.
     
     Delegate to send the received data and data type.
     
     */
     
    
    /**
     Socket Manager Did Receive Data Delegate.
     
     Delegate to send the received data, data type and event Key.
     
     */
    
    func socketManagerDidReceiveData(data: JSON, messageDataType: MessageDataType,forEvent event:EventsTypes, messageID: String)
    
    /**
     Socket Manager Did Receive Message ID Delegate.
     
     Delegate to send the received message ID.
     
     */
    func socketManagerDidReceiveMessageID(messageId: String,data:AnyObject?)
    
 }
 
 
 // MARK: SocketManager Class
 
 class SocketManager: NSObject{
    
    
    // Shared manager - singleton
    static let shared = SocketManager()
    
    // Default time out in seconds
    private let defaultTimeOut: Int = 10
    
    // Default number of reconnect attempts
    private let defaultReconnectAttempts = 1
    
    // Default message ack id
    private let defaultMessageAckID = "-404"
    
    // Default message payload
    let defaultMessagePayload = "{\"data\":\"empty\"}"
    
    // Socket variable
    private var socket: SocketIOClient! = nil
    
    // Ack counter variable
    private var ackCounter: Int = 0
    
    
    let reachability =  Reachability()
    
    // Manager status to keep traking about the connection
    var managerStatus: SocketManagerStatus = .NotConnected {
        didSet {
            
            // If status changed to connected then we add data handlers
            // if status changed to not connected then we remove all handlers
            if(managerStatus == .Connected){
                addDataHandlers()
            }else{
                removeAllHandlers()
            }
        }
    }
    
    // Manager error to keep traking about the errors
    var managerError: SocketManagerError = .NONE
    
    // Manager flag to keep tracking about time out error
    private var isConnectionTimeOut: Bool = false
    
    // Delegate variable
    var delegate: SocketManagerDelegate! = nil
    
    // Debugging mode variable
    var debuggingMode: Bool = false
    
    // Server url
    var serverURL: String
    
    // Connection time out after seconds
    var connectionTimeOutAfter: Int {
        
        didSet{
            
            // Check connection time out after on set
            if(connectionTimeOutAfter < 1){
                socketManagerDidReceiveError(socketManagerError: .ConnectionTimeOutAfterError)
            }
        }
    }
    // Connection reconnect attempts
    var connectionReconnectAttempts: Int {
        
        didSet{
            
            // Connection reconnect attempts on set
            if(connectionReconnectAttempts < 1 && !isConnectionTimeOut){
                socketManagerDidReceiveError(socketManagerError: .ConnectionReconnectAttemptsError)
            }
        }
    }
    
    
    
    // Set defaults
    override init() {
        
        self.serverURL = Constants.socketLink
        self.connectionTimeOutAfter = defaultTimeOut
        self.connectionReconnectAttempts = defaultReconnectAttempts
        self.managerStatus = .NotConnected
        
    }
    
    
    /**
     Socket Manager Log Funcation.
     
     Use this function to log any data for debug.
     
     This function only will print the log if the user define the (debuggingMode) as true.
     
     - Parameter items: Items to be printed.
     - Parameter separator: Separator between printed items (optional).
     - Parameter terminator: Terminator to stop printing (optional).
     
     */
    private func socketManagerLogger(items: Any..., separator: String = "", terminator: String = ""){
        if(debuggingMode){
            print(items,separator,terminator)
        }
    }
    
    
    /**
     Check SocketManager Delegate Funcation.
     
     Use this function to check SocketManager delegate is defind or not.
     
     */
    private func isDelegateDefind() -> Bool{
        
        if(delegate != nil){
            return true
        }
        return false
    }
    
    // Check if the socket is defind
    /**
     Check SocketManager Socket Funcation.
     
     Use this function to check SocketManager socket is defind or not.
     
     - Parameter socketManagerError: Socket manager error to be forward to socket manager did receive error.
     
     */
    private func isSocketDefind(socketManagerError: SocketManagerError) -> Bool{
        
        if(socket != nil){
            return true
        }else{
            socketManagerDidReceiveError(socketManagerError: socketManagerError)
            return false
        }
    }
    
    
    /**
     Get Message Data Type Funcation.
     
     Use this function to get message data type from data.
     
     - Parameter data: Received data.
     
     */
    private func getMessageDataType(data: AnyObject) -> MessageDataType {
        
        let dataArray = data as! Array<AnyObject>
        
        var type = MessageDataType.unknown.rawValue
        
        do{
            try type = dataArray.lookup(index: DefaultIndexForPayload.MessageDataType.rawValue) as! Int
            
        }catch{
            
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            
        }
        
        return getMessageDataTypeWithRawValue(rawValue: type)
        
    }
    
    /**
     Check Message Data Type Funcation.
     
     Use this function to check message data type is correct.
     
     - Parameter dataType: Data type.
     
     */
    private func isMessageDataTypeFound(dataType: MessageDataType) -> Bool{
        
        if let _ = MessageDataType(rawValue: dataType.rawValue){
            return true
        }
        return false
    }
    
    /**
     Check Message Data Type Funcation.
     
     Use this function to check message data type is correct.
     
     - Parameter rawValue: Data type as rawValue.
     
     */
    private func isMessageDataTypeFound(rawValue: Int) -> Bool{
        
        if let _ = MessageDataType(rawValue: rawValue){
            return true
        }
        return false
        
    }
    
    /**
     Get Message Data Type Funcation.
     
     Use this function to get message data type from rawValue.
     
     - Parameter rawValue: Data type as rawValue.
     
     */
    private func getMessageDataTypeWithRawValue(rawValue: Int) -> MessageDataType{
        
        if(isMessageDataTypeFound(dataType:MessageDataType(rawValue: rawValue)!)){
            
            if let dataType = MessageDataType(rawValue: rawValue){
                return dataType
            }
        }
        
        return .unknown
        
    }
    
    /**
     Send Ack To The Server Funcation.
     
     Use this function to send ack and let the server know that client received data.
     
     - Parameter data: Received data.
     - Parameter ack: The call back handler
     
     */
    private func sendAck(data: AnyObject,ack: SocketAckEmitter){
        
        let msgAckID = getAckID(data: data)
        ack.with(msgAckID)
        
    }
    
    private func getAckID(data: AnyObject) -> String {
        
        let dataArray = data as! Array<AnyObject>
        var msgAckID = defaultMessageAckID
        
        do{
            
            try msgAckID = dataArray.lookup(index: DefaultIndexForPayload.MessageAck.rawValue) as! String
            
        }catch{
            
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            return msgAckID
        }
        
        return msgAckID
        
    }
    
    
    private func getMessageID(data: AnyObject) -> String {
        
        let dataArray = data as! Array<AnyObject>
        var msgID = "0"
        
        do{
            
            try msgID = dataArray.lookup(index: DefaultIndexForPayload.MessageID.rawValue) as! String
            
        }catch{
            
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            return msgID
        }
        
        return msgID
        
        
    }
    
    /**
     Parse Response Funcation.
     
     Use this function to get JSON String from received data.
     
     - Parameter data: Received data.
     

    private func parseResponse(data: AnyObject) -> (json: JSON,error: Bool, messageID: String){
        
        
        // No error by default
        var errorFlag: Bool = false
        var jsonObject: JSON = JSON(defaultMessagePayload.data(using: String.Encoding.utf8)!)
        
        let msgID = getMessageID(data: data)
        let dataArray = data as! Array<AnyObject>
        
        do {
            
            let jsonString = try dataArray.lookup(index: DefaultIndexForPayload.MessageData.rawValue) as! [String:Any]
            jsonObject = JSON(jsonString["data"]!)
            
        }catch{
            
            errorFlag = true
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            
        }
        
        return (jsonObject, errorFlag, msgID)
        
    }
         */
    /**
     Socket Manager Did Receive Error Funcation.
     
     Use this delegate to send error message.
     
     - Parameter socketManagerError: Socket manager error to send.
     
     */
    func socketManagerDidReceiveError(socketManagerError: SocketManagerError){
        
        // Set the error
        managerError = socketManagerError
        
        
        // Check if the error is from time out or not
        if(connectionReconnectAttempts > 0 && isConnectionTimeOut){
            
            // This code must be done as this order "Don't change it"
            connectionReconnectAttempts -= 1
            isConnectionTimeOut = false
            openConnection()}
        else{
                delegate?.socketManagerDidReceiveError(error: socketManagerError)}
        
        // Always return false
     //   return false
    }
    
    
    /**
     Adding Data Handler After Connection Successful Funcation.
     
     Use this funcation to add data handlers after connection successful.
     
     */
    private func addDataHandlers(){
        
        socketManagerLogger(items: "Try add data handlers \(#function)")
        
        socket.on(EventsKeys.busLocation.rawValue) { (data, ack) in
            
            print(data)

            
            if let dataFromString = (data[0] as! String).data(using: String.Encoding.utf8, allowLossyConversion: false) {
                let jsonObject = JSON(data: dataFromString)
                self.delegate?.socketManagerDidReceiveData(data: jsonObject, messageDataType: .text, forEvent: EventsTypes.busLocation, messageID: "")

            }

            
        
        }
        
        
    }
    
    /**
     Adding Connection Handler Funcation.
     
     Use this funcation to add handlers for connection.
     
     */
    private func addConnectionHandlers() {
        
        
        socketManagerLogger(items: "Try add connection handlers \(#function)")
        
        
        if(isSocketDefind(socketManagerError: .AddingHandlerError)){
            
            /*   connect  listener
             called when connection is opened */
            
            socket.on(EventsKeys.Connected.rawValue) {data, ack in
                
                print("socket connected now")
                self.managerStatus = .Connected
                self.ackCounter = 0
                self.sendAck(data: data as AnyObject,ack: ack)
                
                 self.delegate?.socketManagerDidConnect()
                
            }


            
            /*  disconnect  listener
             called when session is canceled */
            
            socket.on(EventsKeys.Disconnected.rawValue) {data, ack in
                
                self.managerStatus = .NotConnected
                self.ackCounter = 0
                self.sendAck(data: data as AnyObject,ack: ack)
                self.delegate?.socketManagerDidDisconnect()
                
            }
            
            /*  reconnect  listener
             called when session is reconnected */
            
            socket.on(EventsKeys.Reconnect.rawValue) {data, ack in
                if  self.socket.status == .disconnected{
                    print("disconnected")

                }
                else if self.socket.status == .notConnected{
                    print("notconnected")
                }
                print("Reconnect")
                self.managerStatus = .NotConnected
                 
            }
            socket.on(EventsKeys.ReconnectAttempt.rawValue) {data, ack in
                print("Reconnect")
                self.managerStatus = .NotConnected
                 
            }
            
            /*  error  listener
             called when session is has an error */
            

            socket.on(EventsKeys.Error.rawValue) {data, ack in
                print("Error")
                if let reson = data.last as? String {
                    print("Error reson "+reson)
                    if reson == "Could not connect to the server." || reson == "Tried emitting when not connected" {
                        self.managerStatus = .NotConnected
                        self.delegate?.socketManagerDidDisconnect()

                    }

                }
                
            }
            
        }
    }
    
     
    /**
     Remove Handler Funcation.
     
     Use this funcation to remove handler from scoket.
     
     - Parameter handlerName: Handler name to be removed.
     
     */
    private func removeHandler(handlerName:String){
        
        if(isSocketDefind(socketManagerError: .RemovingHandlerError)){
            
            socket.off(handlerName)
            
        }
        
    }
    
    /**
     Remove All Handlers Funcation.
     
     Use this funcation to remove all handlers from scoket.
     
     */
    private func removeAllHandlers(){
        if(socket != nil){
           // socket.removeAllHandlers()
           //  socket.off(EventsKeys.Error.rawValue)
        }
    }
    
    
    
    internal func validDataBeforeSend(dataType: MessageDataType) -> Bool{
        
        if(!isMessageDataTypeFound(dataType:dataType)){
            socketManagerDidReceiveError(socketManagerError: .SendingDataError)
            return false
        }
        
        if(!isSocketDefind(socketManagerError: .SendingDataError)){
            return false
        }
        
        if(managerStatus == .NotConnected){
            socketManagerDidReceiveError(socketManagerError: .SendingDataError)
            return false
        }
        
        
        return true
        
    }
    
    
    /**
     Open Connection Funcation.
     
     Use this funcation to open connection.
     
     */
    func openConnection(){
        socketManagerLogger(items: "Try open connection \(#function)")
        
        // Updated the link with correct url, So we try to connect
        
        socket = SocketIOClient(socketURL: URL(string: serverURL)!, config: [])

        //** start listening to default events
        addConnectionHandlers()
        socket.connect(timeoutAfter: connectionTimeOutAfter, withHandler: {
            //** connection faid
            self.isConnectionTimeOut = true
            self.closeConnection()
            self.socketManagerDidReceiveError(socketManagerError: .ConnectionTimedOutError)
        })
    }
    
    func emit( messageID: String, dataType: MessageDataType, eventKey: EventsKeys, data: SocketData){
        if self.managerStatus == .NotConnected{
            self.delegate?.socketManagerDidReceive(error: .DisconnectingError , forEvent:eventKey)
        }
            
        else{
            if(!validDataBeforeSend(dataType: dataType)){
                return
            }
            socket.emit(eventKey.rawValue, data)
        }
    }
        
        
    /**
     Send Data Funcation.
     
     Use this funcation to send data over the connection.
     
     - Parameter handlerName: Handler name to be removed.
     - Parameter eventKey: Event name to be send.
     
     */
    func emitWithAck( messageID: String, dataType: MessageDataType, eventKey: EventsKeys, data: SocketData){
        
            if self.managerStatus == .NotConnected{
                self.delegate?.socketManagerDidReceive(error: .DisconnectingError , forEvent:eventKey)
            }
                
            else{
                if(!validDataBeforeSend(dataType: dataType)){
                    return
                }
                
                socket.emitWithAck(eventKey.rawValue, data).timingOut(after: connectionTimeOutAfter, callback: {data in
                    
                    // Send delegate with ack
                    let dataArray = data as Array<AnyObject>
                    var ack = self.defaultMessageAckID
                    
                    var messageID = ""
                    
                    do{
                        //** parse data
                        try ack = dataArray.lookup(index: DefaultIndexForPayload.MessageAck.rawValue) as! String
                        try messageID = dataArray.lookup(index: DefaultIndexForPayload.MessageID.rawValue) as! String
                        
                        if(self.managerStatus == .Connected && ack != self.defaultMessageAckID){
                            if(self.isDelegateDefind()){
                                self.delegate?.socketManagerDidReceiveMessageID(messageId: "\(messageID)",data:nil)
                            }
                        }
                        
                    }catch{
                        self.socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
                    }
                })
                
        }
    }
    
    /**
     Close Connection Funcation.
     
     Use this funcation to close connection.
     
     */
    func closeConnection(){
        
        if(isSocketDefind(socketManagerError: .DisconnectingError)){
            socket.disconnect()
            //removeAllHandlers()
            //socket = nil
        }
    }

    func didFinishLaunchingWithOptions(){
    }
    
    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        //closeConnection()
        
    }
    
    func applicationDidEnterBackground() {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        closeConnection()
    }
    
    func applicationWillEnterForeground() {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        openConnection()
    }
 }
 
 
 
 extension SocketManager {
    
    // MARK: Custom String Convertible Extension
    
    override var description: String {
        var descriptors: [String] = []
        
        switch self.managerStatus {
        case .NotConnected:
            descriptors.append("Not connected")
        case .Connected:
            descriptors.append("Connected")
        }
        
        switch self.managerError {
            
        case .NONE:
            descriptors.append("Not an error")
        case .ConnectingError:
            descriptors.append("Connection can not be opened, try to change serverURL")
        case .DisconnectingError:
            descriptors.append("Disconnecting failed")
        case .AddingHandlerError:
            descriptors.append("Adding handler failed")
        case .RemovingHandlerError:
            descriptors.append("Removing handler failed")
        case .SendingDataError:
            descriptors.append("Sending data failed")
            
        case .DataTypeUnknownError:
            descriptors.append("Unknown data type from server")
            
        case .NoInternetConnectionError:
            descriptors.append("No Internet connection, please check your connectivity")
        case .ConnectionTimedOutError:
            descriptors.append("Connection timedout, please check server side or check serverURL")
            
        case .ConnectionReconnectAttemptsError:
            descriptors.append("Please enter number 1 or above above, default is 1")
            
        case .ConnectionTimeOutAfterError:
            descriptors.append("Please enter number 1 or above above, default is 10 seconds")
            
             
        case .ArrayIndexOutOfBoundsError:
            descriptors.append("Array index out of bounds")
            
        case .BuildingJsonPayloadError:
            descriptors.append("Something went wrong while building json payload")
            
        case .dataNotReceived:
            descriptors.append("Data Not Received From server")
  
        }
        
        return descriptors.joined(separator: ", ")
        
    }
    
    // MARK: Build Json String Payload Extension
    
    func buildJsonStringPayload(item: AnyObject, messageDataType: MessageDataType) -> AnyObject?{
        
        var json:AnyObject? = nil
        
        switch messageDataType {
        case .text:
            
            json = ["data":item] as AnyObject
            
        case .image:
            
            break
        default:
            
            break
            
        }
        
        return json
        
    }
    
    
    
    private func parseResponse(data: AnyObject) -> (json: JSON,error: Bool, messageID: String){
        
        
        // No error by default
        var errorFlag: Bool = false
        var jsonObject: JSON = JSON(defaultMessagePayload.data(using: String.Encoding.utf8)!)
        
        let msgID = getMessageID(data: data)
        let dataArray = data as! Array<AnyObject>
        
        do {
            
            let jsonString = try dataArray.lookup(index: DefaultIndexForPayload.MessageData.rawValue) as! [String:String]
            //     jsonObject = JSON(jsonString["data"]!)
            
            if let dataFromString = jsonString["data"]?.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                jsonObject = JSON(data: dataFromString)
            }
            
        }catch{
            
            errorFlag = true
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            
        }
        
        return (jsonObject, errorFlag, msgID)
        
    }
    
    private func getMessageID(data: AnyObject) -> String {
        
        let dataArray = data as! Array<AnyObject>
        var msgID = "0"
        
        do{
            
            try msgID = dataArray.lookup(index: DefaultIndexForPayload.MessageID.rawValue) as! String
            
        }catch{
            
            socketManagerDidReceiveError(socketManagerError: .ArrayIndexOutOfBoundsError)
            return msgID
        }
        
        return msgID
        
        
    }
    
    
 }
 
 // MARK: Array Extension
 extension Array {
    func lookup(index : Int) throws -> Element {
        if index >= count {throw
            NSError(domain: "com.virtecha", code: 0,
                    userInfo: [NSLocalizedFailureReasonErrorKey:
                        "Array index out of bounds"])}
        return self[Int(index)]
    }
 }
 
 // MARK: String Extension
 extension String {
    
    func escapeString() -> String {
        
        var newString = self.replacingOccurrences(of:"\n", with: "\\n")
        newString = newString.replacingOccurrences(of:"\t", with: "\\t")
        newString = newString.replacingOccurrences(of:"\r", with: "\\r")
        
        return newString
    }
    
    func decodingImageBase64() -> UIImage {
        
        let data = NSData(base64Encoded: self, options:NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: data as Data)!
        
    }
    
 }
 
 
