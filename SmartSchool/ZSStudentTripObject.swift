//
//  ZSStudentTripObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSStudentTripObject: NSObject {
    
    var _id              = String()
    var __v              = String()
    var student_id       = [String]()
    var points :[ZSSubPointObject]?   = [ZSSubPointObject]()

    override init() { }
    
    init(studentTrip:JSON) {
        super.init()
        
        _id   = studentTrip["_id"].stringValue
        __v   = studentTrip["__v"].stringValue
        
        for temp in studentTrip["points"].arrayValue {
            let subPointObject = ZSSubPointObject(subPoint: temp)
            points?.append(subPointObject)
        }
    }
}
