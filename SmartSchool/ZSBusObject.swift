//
//  ZSBusObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSBusObject: NSObject {
    
    var accuracy         = Double()
    var busId            = String()
    var busNumber        = String()
    var latitude         = Double()
    var longitude        = Double()
    var speed            = Double()
    var status           = Int()
    var studentsInBus    = Int()
    var time             = Double()
    var totalStudents    = Int()

     
    override init() { }
    
    init(bus:JSON) {
        super.init()
        
        accuracy              = bus["accuracy"].doubleValue
        busId                 = bus["busId"].stringValue
        busNumber             = bus["busNumber"].stringValue
        latitude              = bus["latitude"].doubleValue
        longitude             = bus["longitude"].doubleValue
        speed                 = bus["speed"].doubleValue
        status                = bus["status"].intValue
        studentsInBus         = bus["studentsInBus"].intValue
        time                  = bus["time"].doubleValue
        totalStudents         = bus["totalStudents"].intValue

    }
}
