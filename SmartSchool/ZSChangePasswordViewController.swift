//
//  ZSChangePasswordViewController.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON
import TPKeyboardAvoiding

class ZSChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    @IBOutlet weak var oldPasswordTextfield: UITextField!
    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    
    @IBOutlet weak var oldPasswordLineDividerView: UIView!
    @IBOutlet weak var newPasswordLineDividerView: UIView!
    @IBOutlet weak var confirmPasswordLineDividerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "تغيير كلمة السر"
//        var image = UIImage(named: "logout.png")
//        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.logout))
        let doneButton   = UIBarButtonItem(title: "تم",  style: .plain, target: self, action: #selector(self.setPassword))
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.oldPasswordTextfield.delegate     = self
        self.newPasswordTextfield.delegate     = self
        self.confirmPasswordTextfield.delegate = self
        
        self.oldPasswordTextfield.attributedPlaceholder = NSAttributedString(string:"كلمة السر القديمة", attributes: [NSForegroundColorAttributeName: UIColor.lightText])

        self.newPasswordTextfield.attributedPlaceholder = NSAttributedString(string:"كلمة السر الجديدة", attributes: [NSForegroundColorAttributeName: UIColor.lightText])

        self.confirmPasswordTextfield.attributedPlaceholder = NSAttributedString(string:"تأكيد كلمة السر", attributes: [NSForegroundColorAttributeName: UIColor.lightText])
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == -10 { //Old Password
            self.oldPasswordLineDividerView.backgroundColor = UIColor.orange
        } else if textField.tag == -20 { //New Password
            self.newPasswordLineDividerView.backgroundColor = UIColor.orange
        } else if textField.tag == -30 { //Confirm New Password
            self.confirmPasswordLineDividerView.backgroundColor = UIColor.orange
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == -10 { //Old Password
            self.oldPasswordLineDividerView.backgroundColor = UIColor.gray
        } else if textField.tag == -20 { //New Password
            self.newPasswordLineDividerView.backgroundColor = UIColor.gray
        } else if textField.tag == -30 { //Confirm New Password
            self.confirmPasswordLineDividerView.backgroundColor = UIColor.gray
        }    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        self.setPassword()
    }
    

    @IBAction func cancelAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func logout(){
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func isValidPassword()->Bool {
        
        if self.newPasswordTextfield.text == "" || self.confirmPasswordTextfield.text == "" || self.oldPasswordTextfield.text == "" {
            Util.showInfoAlert(msg: "الرجاء ملء كل الحقول ثم المحاولة مرة أخرى", controller: self, callback: { (success) in
            })
//            KVNProgress.showError(withStatus: "الرجاء ملء كل الحقول ثم المحاولة مرة أخرى")
            return false
        }
        
        if self.newPasswordTextfield.text != self.confirmPasswordTextfield.text {
            
            Util.showInfoAlert(msg: "كلمة المرور وتأكيدها غير متوافقان، الرجاء المحاولة مرة أخرى", controller: self, callback: { (success) in
                self.confirmPasswordTextfield.becomeFirstResponder()
                self.confirmPasswordTextfield.text = ""
                self.confirmPasswordLineDividerView.backgroundColor = UIColor.red
            })

            
//            KVNProgress.showError(withStatus: "كلمة المرور وتأكيدها غير متوافقان، الرجاء المحاولة مرة أخرى", completion: {
//                self.confirmPasswordTextfield.becomeFirstResponder()
//                self.confirmPasswordTextfield.text = ""
//                self.confirmPasswordLineDividerView.backgroundColor = UIColor.red
//            })
            return false
        }
        return true
    }
    
    func setPassword() {
        view.endEditing(true)
 
        if !isValidPassword() {
            return
        }
        
        self.oldPasswordTextfield.text = Util.replaceArabicNumberWithEnglishFrom(number: self.oldPasswordTextfield.text!)
        
        self.newPasswordTextfield.text = Util.replaceArabicNumberWithEnglishFrom(number: self.newPasswordTextfield.text!)
        
        self.confirmPasswordTextfield.text = Util.replaceArabicNumberWithEnglishFrom(number: self.confirmPasswordTextfield.text!)
        
        let token: String     = UserDefaults.standard.string(forKey: "token")!
        let userID: String    = UserDefaults.standard.string(forKey: "userID")!

        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]
        
        let parameters = ["user_id": userID,
                          "old_password": "\(self.oldPasswordTextfield.text!)",
                          "new_password": "\(self.newPasswordTextfield.text!)",
                          "new_password_confirm": "\(self.confirmPasswordTextfield.text!)"]
        
        self.loading.isHidden = false
//        self.doneButton.isHidden = true
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_CHANGE_PASSWORD, parameters: parameters, headers: headers) { (data) in
            print(data ?? "")
            
            if data != nil {
                self.loading.stopAnimating()

                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    
                    Util.showInfoAlert(msg: "تم تعديل كلمة المرور بنجاح", controller: self, callback: { (success) in
                        KeychainWrapper.standard.set(self.newPasswordTextfield.text!, forKey: "password")
                        self.navigationController?.popViewController(animated: true)
                    })
                    
//                    KVNProgress.showSuccess(withStatus: "تم تعديل كلمة المرور بنجاح", completion: {
//                        self.navigationController?.popViewController(animated: true)
//                    })
                }  else {
                    self.loading.stopAnimating()
                    
                    if jsonData["message"].stringValue == "wrong old pasword" {
                        
                        Util.showInfoAlert(msg: "كلمة المرور الحالية غير صحيحة، الرجاء المحاولة مرة أخرى", controller: self, callback: { (success) in
                            self.oldPasswordTextfield.becomeFirstResponder()
                        })
                        
//                        KVNProgress.showError(withStatus: "كلمة المرور الحالية غير صحيحة، الرجاء المحاولة مرة أخرى", completion: {
//                            self.oldPasswordTextfield.becomeFirstResponder()
//                        })
                    } else if jsonData["message"].stringValue == "New Password is the same Old Password" {
                        Util.showInfoAlert(msg: "كلمة المرور الجديدة هي نفس كلمة المرور القديمة", controller: self, callback: { (success) in
                        })
//                        KVNProgress.showError(withStatus: "كلمة المرور الجديدة هي نفس كلمة المرور القديمة")
                    } else {
                        Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                            self.oldPasswordTextfield.becomeFirstResponder()
                        })
//                        KVNProgress.showError(withStatus: jsonData["message"].stringValue)
                    }
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
