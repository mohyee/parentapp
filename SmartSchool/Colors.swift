//
//  Colors.swift
//  YaqutSwift
//
//  Created by Amer Smadi on 11/10/14.
//  Copyright (c) 2014 Kindi. All rights reserved.
//

import UIKit

open class Colors {
    
    static let navColor: UInt = 0xD8D8D8
    static let tabSelectedColor: UInt = 0xEC1516
    static let tabUnselectedColor: UInt = 0x5A5A5A
    static let searchBarTintColor: UInt = 0x9B9B9B
    static let linkColor : UInt = 0x0076FF
    static let primaryJarirRedColor : UInt = 0xD4070E
    static let greenColor : UInt = 0x3F9A52

    static let addToCartColor: UInt = 0xD4070E
    static let preOrderColor: UInt = 0xFFC107
    static let checkInStoreColor: UInt = 0x007BC9
    static let outOfStockColor: UInt = 0xE0E0E0
    static let addedToCartColor: UInt = 0x3F9A52
    
    
    static let catsColors: [UInt] = [0xD53939, 0x1D6DCA, 0x4564CA, 0x939597, 0x2FA555, 0x2FA597, 0x148747, 0xEFC036, 0xE3A430, 0xE97939, 0xF69431, 0xA7B844]
    
    open static func colorFromHexString(_ hexString: String) -> UIColor {
        var rgbValue: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 0
        scanner.scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    open static func colorFromHex(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    open static func colorFromHexWithAlpha(_ rgbValue: UInt, alpha: CGFloat) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    open static func colorFromHexString(hex: String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
