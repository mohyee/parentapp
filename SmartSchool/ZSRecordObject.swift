//
//  ZSRecordObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSRecordObject: NSObject {

    var student_id        = String()
    var title             = String() //(title of inserted point)
    var type              = String() //JSON Object which contains an integer "id" as point type 
    var longitude         = String()
    var latitude          = String()
    var isPrimary         = Bool()
    
    override init() { }
    
    init(record:JSON) {
        super.init()
        
        student_id            = record["_id"].stringValue
        title                 = record["title"].stringValue
        type                  = record["type"].stringValue
        longitude             = record["longitude"].stringValue
        latitude              = record["latitude"].stringValue
        isPrimary             = record["isPrimary"].boolValue
    }
}
