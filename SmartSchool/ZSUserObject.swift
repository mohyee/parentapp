//
//  ZSUserObject.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/11/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSUserObject: NSObject {

    var _id              = String()
    var created_at       = String()
    var updated_at       = String()
    var name             = String()
    var hashedPassword   = String()
    var type             = String()
    var refid            = String()
    var email            = String()
    var phone_number     = String()
    var __v              = Int() //Version
    var school_id        = [String]()
    var app_device :[ZSAppDeviceObject]?   = [ZSAppDeviceObject]()

    override init() { }
    
    init(user:JSON) {
        super.init()
        
        _id                   = user["_id"].stringValue
        created_at            = user["created_at"].stringValue
        updated_at            = user["updated_at"].stringValue
        name                  = user["name"].stringValue
        hashedPassword        = user["hashedPassword"].stringValue
        type                  = user["type"].stringValue
        refid                 = user["refid"].stringValue
        email                 = user["email"].stringValue
        phone_number          = user["phone_number"].stringValue
        __v                   = user["__v"].intValue
        school_id             = user["school_id"].arrayObject as! [String]
    
        for temp in user["app_device"].arrayValue {
            let appDeviceObject = ZSAppDeviceObject(appDevice: temp)
            app_device?.append(appDeviceObject)
        }
    }
}

  
//    "user": {
//        "_id": "5872181908db91dc77e896c8",
//        "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "updated_at": "Wed Feb 01 2017 15:45:12 GMT+0000 (UTC)",
//        "name": "salm sari yaser",
//        "hashedPassword": "/wEIiSaVrBqTpowCwqL1J/18161o0XrERCqcZ03HEKRCy7OTYQ2BOm1ckwstVrstpb8ExfPatcW9gnMWxSSKpA==",
//        "type": "1",
//        "refid": "5872181908db91dc77e896c7",
//        "email": "hayasalm@yahoo.com",
//        "phone_number": "1234567866",
//        "__v": 0,
//        "app_device": {
//            "app_code": 1,
//            "app_version": "1.0",
//            "device_language": "ar",
//            "device_manufacture": "samsung",
//            "device_model": "SM-N920C",
//            "device_token": "ewLaUDT9ahA:APA91bEYn0JVBGKYA2wc1gOuIY3n3VhkeW6FFqIQ8SVGibnhLBDFy74UgWBAvtzDEeV7ch_ePJYjuRpbIj2mQt2_P1gi2FtiIg-TQ_xDYtya42rrGY_rVECBZujHSSDH2dhRtAKEZiQ7",
//            "device_type": "Android",
//            "device_uuid": "a450488f-67d3-457a-8db5-1e08932eec4b",
//            "ip": "10.164.123.169",
//            "latitude": "0.0",
//            "longitude": "0.0",
//            "os_name": "android",
//            "os_version": "6.0.1"
//        },
//        "school_id": [
//        "587153e429a577d62e2cae2f"
//        ]
//    }
