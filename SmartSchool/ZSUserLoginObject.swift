//
//  ZSUserLoginObject.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSUserLoginObject: NSObject {
    
    var status  = String()
    var success = Bool()
    var message = String()
    var token   = String()
    var parent:[ZSParentObject]?   = [ZSParentObject]()
    var user:[ZSUserObject]?       = [ZSUserObject]()
    var student:[ZSStudentObject]? = [ZSStudentObject]()
    
    override init() { }
    
    init(userLogin:JSON){
        
        status  = userLogin["status"].stringValue
        success = userLogin["success"].boolValue
        message = userLogin["message"].stringValue
        token   = userLogin["token"].stringValue
        
        for temp in userLogin["parent"].arrayValue {
            let parentObject = ZSParentObject(parent: temp)
            parent?.append(parentObject)
        }
        
        for temp in userLogin["user"].arrayValue {
            let userObject = ZSUserObject(user: temp)
            user?.append(userObject)
        }
        
        for temp in userLogin["students"].arrayValue {
            let studentObject = ZSStudentObject(student: temp)
            student?.append(studentObject)
        }
    }
}
//{
//    "status": "success",
//    "success": true,
//    "message": "success",
//    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfYnNvbnR5cGUiOiJPYmplY3RJRCIsImlkIjoiWHJcdTAwMThcdTAwMTlcYsObwpHDnHfDqMKWw4giLCJpYXQiOjE0ODY3MzU3MjcsImV4cCI6MTQ4NzM0MDUyN30.NPpbCuiKOLFuObZCyOPTR7nz8h-XuNRR2-OVjWywevw",
//    "parent": {
//        "_id": "5872181908db91dc77e896c7",
//        "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "updated_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "first_name": "salm",
//        "middle_name": "sari",
//        "last_name": "yaser",
//        "__v": 0,
//        "school_id": [
//        "587153e429a577d62e2cae2f"
//        ],
//        "text": "salm sari yaser",
//        "id": "5872181908db91dc77e896c7"
//    },
//    "user": {
//        "_id": "5872181908db91dc77e896c8",
//        "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "updated_at": "Wed Feb 01 2017 15:45:12 GMT+0000 (UTC)",
//        "name": "salm sari yaser",
//        "hashedPassword": "/wEIiSaVrBqTpowCwqL1J/18161o0XrERCqcZ03HEKRCy7OTYQ2BOm1ckwstVrstpb8ExfPatcW9gnMWxSSKpA==",
//        "type": "1",
//        "refid": "5872181908db91dc77e896c7",
//        "email": "hayasalm@yahoo.com",
//        "phone_number": "1234567866",
//        "__v": 0,
//        "app_device": {
//            "app_code": 1,
//            "app_version": "1.0",
//            "device_language": "ar",
//            "device_manufacture": "samsung",
//            "device_model": "SM-N920C",
//            "device_token": "ewLaUDT9ahA:APA91bEYn0JVBGKYA2wc1gOuIY3n3VhkeW6FFqIQ8SVGibnhLBDFy74UgWBAvtzDEeV7ch_ePJYjuRpbIj2mQt2_P1gi2FtiIg-TQ_xDYtya42rrGY_rVECBZujHSSDH2dhRtAKEZiQ7",
//            "device_type": "Android",
//            "device_uuid": "a450488f-67d3-457a-8db5-1e08932eec4b",
//            "ip": "10.164.123.169",
//            "latitude": "0.0",
//            "longitude": "0.0",
//            "os_name": "android",
//            "os_version": "6.0.1"
//        },
//        "school_id": [
//        "587153e429a577d62e2cae2f"
//        ]
//    },
//    "students": [
//    {
//    "_id": "5872181908db91dc77e896c9",
//    "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//    "updated_at": "Wed Feb 01 2017 12:26:26 GMT+0000 (UTC)",
//    "first_name": "haya",
//    "father_name": "salm ",
//    "grand_father_name": "sari",
//    "last_name": "yaser",
//    "national_id": "",
//    "__v": 0,
//    "educational_documents": [],
//    "classroom_id": [
//    "587214e008db91dc77e89635"
//    ],
//    "school_id": [
//    "587153e429a577d62e2cae2f"
//    ],
//    "roles": [],
//    "parent_ids": [
//    "5872181908db91dc77e896c7"
//    ],
//    "personal_documents": [],
//    "text": "haya salm  sari yaser",
//    "id": "5872181908db91dc77e896c9"
//    }
//    ]
//}
