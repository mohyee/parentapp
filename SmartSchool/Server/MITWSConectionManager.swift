//
//  MITWSConectionManager.swift
//  TrafficApp
//
//  Created by admin on 10/5/16.
//  Copyright © 2016 MIT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MITWSConectionManager: NSObject {
    
    public enum ResponceFormat {
        case json
        case xml
    }
    
    public enum RequestEncode {
        case json
        case queryString
    }
    
    
    var requestEncoding :URLEncoding? = nil
    var defaultRequestEncoding :ParameterEncoding!
    
    
    override init() {
        
    }
    
    
    init(defaultRequestEncoding :ParameterEncoding) {
        self.defaultRequestEncoding = defaultRequestEncoding
    }
    
    
    // Shared instance
    //please note that to change "defaultRequestEncoding" as api's requirement
    
    static let shared = MITWSConectionManager(defaultRequestEncoding: JSONEncoding.default)
    
    //MARK:- ****** REQUESTS ******
    
    //MARK:- POST Connection
    func post(url:String,parameters:Parameters?,requestEncoding:URLEncoding,responceFormat:ResponceFormat)  {
        if responceFormat == .json {
            postJSONResponse(url: url, parameters: parameters, requestEncoding: requestEncoding, completion: { (result) in
                
            })
            
        }
        else {
            postXMLResponse(url: url, parameters: parameters, requestEncoding: requestEncoding)
        }
    }
    
    //MARK: Post JSON
    //*perform connection which return JSON format
    func postJSONResponse(url:String,parameters:Parameters?,requestEncoding:URLEncoding,completion:@escaping (_ result:AnyObject?)-> Void)  {
        
        ///-set requestEncoding value to ignore defualt encode
        self.requestEncoding = requestEncoding
        postJSONResponse(url: url, parameters: parameters) { (result) in
            completion(result)
        }
        
    }
    
    func postJSONResponse(url:String,parameters:Parameters?,headers:HTTPHeaders?,completion:@escaping (_ result:Any?)-> Void){
        
        ///-perform request with requestEncoding if it was set
        //* else  perform it using defulat encode
        
        if parameters != nil {
            print("URL: ",url)
            print("Post Parameters: ",parameters!)
        }

        let currentRequestEncoding = getRequstEncoding()
        
        Alamofire.request(url, method: .post, parameters: parameters!, encoding: currentRequestEncoding, headers:headers ).responseJSON { (response) in
            
            if  response.result.isSuccess {
                if let json = response.result.value {
                    print("JSON: \(json)")
                    completion(json)
                    return
                }
            }
            else if response.result.error != nil{
                print(response.result.error!.localizedDescription)
            }
            completion(nil)
        }
    }

    
    func postJSONResponse(url:String,parameters:Parameters?,completion:@escaping (_ result:AnyObject?)-> Void){
        
        ///-perform request with requestEncoding if it was set
        //* else  perform it using defulat encode
        
        if parameters != nil {
            print("Post para",parameters)}
        
 
        let currentRequestEncoding = getRequstEncoding()
        Alamofire.request(url, method: .post, parameters: parameters!, encoding: currentRequestEncoding, headers: ["Content-Type": "application/json","x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfYnNvbnR5cGUiOiJPYmplY3RJRCIsImlkIjoiWMKDJDlcXMOBwrHCsmPDicKIw78iLCJpYXQiOjE0OTA4ODEwMTksImV4cCI6MTQ5MTQ4NTgxOX0.U7v5EAqUK13H0sv6IYz5mdarGVEKZGSzQmX6M8kFoyw"]).responseJSON { (response) in
            if  response.result.isSuccess {
                if let json = response.result.value {
                    print("JSON: \(json)")
                    
                    completion(json as AnyObject?)
                    return
                }
                
            }
            else if response.result.error != nil{
                print(response.result.error?.localizedDescription)
            }
            completion(nil)
            
        }
        
        
    }
    
    //MARK: Post XML
    //*perform connection which return XML format
    
    func postXMLResponse(url:String,parameters:Parameters?,requestEncoding:URLEncoding)  {
        ///-set requestEncoding value to ignore defualt encode
        self.requestEncoding = requestEncoding
        postXMLResponse(url: url, parameters: parameters) { (result) in
            
        }
        
    }
    
    
    func postXMLResponse(url:String,parameters:Parameters?,completion:@escaping (_ result:AnyObject?)-> Void){
        ///-perform request with requestEncoding if it was set
        //* else  perform it using defulat encode
        
        let currentRequestEncoding = getRequstEncoding()
        
        print("XML parameters"+"\(parameters)")

        Alamofire.request(url, method: .post, parameters: parameters, encoding: currentRequestEncoding, headers: nil).responsePropertyList { (response) in
            if  response.result.isSuccess {
                if let xmlData = response.result.value {
                    print("XML: \(xmlData)")
                    completion(xmlData as AnyObject?)
                }
                
            }
            else{
                print("Error "+response.result.error!.localizedDescription)
                completion(nil)
            }
            
        }
      
        
     
    }
    
    
    //MARK:- GET Connection
    
    func getJSONResponse(url:String ,completion:@escaping (_ result:AnyObject?)-> Void){
        
        print("URL: "+url)
        ///-perform request with requestEncoding if it was set
        //* else  perform it using defulat encode
        
        Alamofire.request(url).responseJSON { (response) in
            if  response.result.isSuccess {
                if let JSON = response.result.value {
                    completion(JSON as AnyObject!)
                    
                }
            }
                
            else{
                completion(nil)
            }
        }
        
        
    }
    
    
    //MARK:- ******Helping methods******
    private func getRequstEncoding()->ParameterEncoding  {
        
        var currentRequestEncoding = defaultRequestEncoding
        
        if requestEncoding != nil {
            currentRequestEncoding = requestEncoding
            requestEncoding = nil
        }
        
        return currentRequestEncoding!
    }
}
