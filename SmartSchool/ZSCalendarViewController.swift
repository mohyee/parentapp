//
//  ZSCalendarViewController.swift
//  SmartSchool
//
//  Created by Mo7yee on 3/3/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ZSCalendarViewController: UIViewController {
    
    @IBOutlet weak var startDatePickerView: UIDatePicker!
    @IBOutlet weak var endDatePickerView: UIDatePicker!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    var selectedSubPoint = ZSSubPointObject()
    var selectedPointID = String()
    
    let currentDate = Date()
    let deFormatter = DateFormatter()

    var startDateInMillis: Int64? = nil
    var endDateInMillis:   Int64? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "تغيير وقت النقطة"
        let doneButton   = UIBarButtonItem(title: "تم",  style: .plain, target: self, action: #selector(self.doneAction))
        self.navigationItem.rightBarButtonItem = doneButton
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        print("Current Date: \(self.currentDate)")
        print("Selected SubPoint: \(self.selectedSubPoint.title)")
        print("Selected Point ID: \(self.selectedPointID)")

        let gregorian   = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components  = NSDateComponents()
        components.year = 1
        
        self.startDateInMillis   = self.convertTimeMillis(selectedDate: self.currentDate as NSDate)
        self.startDateLabel.text = "Start Date:   \(self.currentDate)"
        
        self.deFormatter.dateFormat = "dd-MM-yyyy"
        self.deFormatter.locale     =  NSLocale(localeIdentifier: "ar_JO") as Locale!
        
        let startDate = deFormatter.string(from: self.currentDate)
        self.startDateLabel.text = "تاريخ البداية:   \(startDate)"
        
        let maxDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))!
        
        self.startDatePickerView.minimumDate = self.minDate(date: self.currentDate)
        self.startDatePickerView.maximumDate = maxDate
        
        self.endDatePickerView.minimumDate   = self.minDate(date: self.currentDate)
        self.endDatePickerView.maximumDate   = maxDate
        
        self.startDatePickerView.locale =  NSLocale(localeIdentifier: "ar_JO") as Locale!
        self.endDatePickerView.locale   =  NSLocale(localeIdentifier: "ar_JO") as Locale!
    }
    
    func minDate(date: Date) -> Date {
        
        let gregorian   = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components  = NSDateComponents()
        components.year = 0
        
        let minDate = gregorian.date(byAdding: components as DateComponents, to: date, options: NSCalendar.Options(rawValue: 0))!
        
        return minDate
    }
    
    @IBAction func startDatePickerAction(_ sender: UIDatePicker) {
        print(sender.calendar)
        
        self.endDatePickerView.minimumDate = self.minDate(date: sender.date)
    
        self.startDateInMillis = self.convertTimeMillis(selectedDate: sender.date as NSDate)
        self.startDateLabel.text = "Start Date:   \(sender.date)"

        let startDate = self.deFormatter.string(from: sender.date)
        print(startDate) // 2015-06-25 23:10:00
        
        self.startDateLabel.text = "تاريخ البداية:   \(startDate)"
    }
    
    @IBAction func endDatePickerAction(_ sender: UIDatePicker) {
        print(sender.calendar)
        
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: sender.date)
        
        // Change the time to 23:59:00 in your locale
        components.hour = 23
        components.minute = 59
        components.second = 0
        
        let date = gregorian.date(from: components)!
        
        self.endDateInMillis = self.convertTimeMillis(selectedDate: date as NSDate)
        
        let endDate = self.deFormatter.string(from: date)
        print(endDate)
        
        self.endDateLabel.text = "تاريخ الانتهاء:   \(endDate)"
    }
    
    func doneAction() {

        if self.endDateInMillis == nil || self.startDateInMillis == nil {
            Util.showInfoAlert(msg: "يرجى تحديد الفترة الزمنية للنطقة لاتمام العملية", controller: self, callback: { (success) in
                return
            })
        }
        
        print("Start Date in Millis: \(self.startDateInMillis)")
        print("End Date in Millis: \(self.endDateInMillis)")
        
        self.isValidDate()
    }
    
    func convertTimeMillis(selectedDate: NSDate) -> Int64{
        let convertedDate = selectedDate.timeIntervalSince1970
        return Int64(convertedDate*1000)
    }
    
    func isValidDate() {
        
        if self.isSubPointActive(activeForObj: self.selectedSubPoint.activeFor) {
            self.callCancelActivePointWithDate(startDate: self.selectedSubPoint.activeFor!.start!, endDate: self.selectedSubPoint.activeFor!.end!)
        } else {
            self.callSetActivePointWithNewDate()
        }
        
        
        if self.selectedSubPoint.activeFor != nil {
            self.callCancelActivePointWithDate(startDate: self.selectedSubPoint.activeFor!.start, endDate: self.selectedSubPoint.activeFor!.end)
        } else {
            self.callSetActivePointWithNewDate()
        }
    }
    
    func callSetActivePointWithNewDate() {
        
        let token: String      = UserDefaults.standard.string(forKey: "token")!
        let pointID: String    = self.selectedPointID
        let subPointID: String = self.selectedSubPoint._id
        
        let urlString = Constants.URL_SET_ACTIVE_FOR
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]

        let params = [Constants.PARAM_POINT_ID:     pointID,
                      Constants.PARAM_SUB_POINT_ID: subPointID,
                      Constants.PARAM_START:        "\(self.startDateInMillis)",
                      Constants.PARAM_END:          "\(self.endDateInMillis)"]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: urlString, parameters: params, headers: headers) { (data) in
            if data != nil {
                self.loading.stopAnimating()
                
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData["success"].boolValue)
//                    for temp in jsonData["students"].arrayValue {
//                        let studentObject = ZSStudentObject(student: temp)
//                        self.studentsData?.append(studentObject)
//                    }
                } else {
                    self.loading.stopAnimating()
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                    })
//                    KVNProgress.showError(withStatus: jsonData["message"].stringValue)
                }
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", controller: self, callback: { (success) in
                })
                print("Connection Error")
            }
        }
    }
    
    func callCancelActivePointWithDate(startDate: Int64, endDate: Int64) {
        
        let token: String      = UserDefaults.standard.string(forKey: "token")!
        let pointID: String    = self.selectedPointID
        let subPointID: String = self.selectedSubPoint._id

        let urlString = Constants.URL_CANCEL_ACTIVE_FOR
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]
        
        let params = [Constants.PARAM_POINT_ID:     pointID,
                      Constants.PARAM_SUB_POINT_ID: subPointID,
                      Constants.PARAM_START:        String(startDate),
                      Constants.PARAM_END:          String(endDate)]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: urlString, parameters: params, headers: headers) { (data) in
            if data != nil {
                self.loading.stopAnimating()
                
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData["success"].boolValue)
                    self.callSetActivePointWithNewDate()
                    //                    for temp in jsonData["students"].arrayValue {
                    //                        let studentObject = ZSStudentObject(student: temp)
                    //                        self.studentsData?.append(studentObject)
                    //                    }
                    
                    
                } else {
                    self.loading.stopAnimating()
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                    })
//                    KVNProgress.showError(withStatus: jsonData["message"].stringValue)
                }
            } else {
                self.loading.stopAnimating()
                Util.showInfoAlert(msg: "Connection Error", controller: self, callback: { (success) in
                })
                print("Connection Error")
            }
        }
    }
    
    func isSubPointActive(activeForObj: ZSActiveForObject?)-> Bool {
        
        var isActiveFor = false
        
        if (activeForObj == nil || activeForObj?.start == 0 || activeForObj?.end == 0 ) {
            return false
        }
        
        let currentDate = Date()
        let convertedDate = currentDate.timeIntervalSince1970
        let currentDateInMillis =  Int64(convertedDate*1000)
        
        let startDate = (activeForObj?.start)!
        let endDate   = (activeForObj?.end)!
        
        if (currentDateInMillis >= startDate && currentDateInMillis <= endDate) {
            isActiveFor = true
        }
        
        return isActiveFor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
