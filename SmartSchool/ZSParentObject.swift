//
//  ZSParentObject.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSParentObject: NSObject {
    
    var parent_id   = String()
    var created_at  = String()
    var updated_at  = String()
    var first_name  = String()
    var middle_name = String()
    var last_name   = String()
    var __v         = Int() //Version
//    var school_id   = [String]()
    var school_id:  [String]?

    var text        = String()
    var id          = String()
    
    override init() {
        
    }
    
    init(parent:JSON) {
        super.init()
        
        parent_id   = parent["_id"].stringValue
        created_at  = parent["created_at"].stringValue
        updated_at  = parent["updated_at"].stringValue
        first_name  = parent["first_name"].stringValue
        middle_name = parent["middle_name"].stringValue
        last_name   = parent["last_name"].stringValue
        __v         = parent["__v"].intValue
//        if parent["school_id"].arrayObject != nil {
//            school_id   = parent["school_id"].arrayObject as? [String]
//        }
        school_id   = parent["school_id"].arrayObject as? [String]

        text        = parent["text"].stringValue
        id          = parent["id"].stringValue
    }
}

//    "parent": {
//        "_id": "5872181908db91dc77e896c7",
//        "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "updated_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//        "first_name": "salm",
//        "middle_name": "sari",
//        "last_name": "yaser",
//        "__v": 0,
//        "school_id": [
//        "587153e429a577d62e2cae2f"
//        ],
//        "text": "salm sari yaser",
//        "id": "5872181908db91dc77e896c7"
//    }
    
