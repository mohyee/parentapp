//
//  ZSTripObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSTripObject: NSObject {
    
    var _id              = String()
    var title            = String()
    var type             = String() //(Map point type)
    var active           = Bool()
    var point_ids        = [String]()
    var subPoint_ids     = [String]()
    var bus_id           = [String]()
    var text             = String()

    override init() { }
    
    init(trip:JSON) {
        super.init()
        
        _id                   = trip["_id"].stringValue
        title                 = trip["title"].stringValue
        type                  = trip["type"].stringValue
        active                = trip["active"].boolValue
        point_ids             = trip["point_ids"].arrayObject as! [String]
        subPoint_ids          = trip["subPoint_ids"].arrayObject as! [String]
        bus_id                = trip["bus_id"].arrayObject as! [String]
        text                  = trip["text"].stringValue
    }
}
