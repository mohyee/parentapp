//
//  ZSSubPointObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSSubPointObject: NSObject {
    
    var created_at        = String()
    var updated_at        = String()
    var title             = String()
    var type              = Int() //(Map point type)
    var province          = String()
    var street_name       = String()
    var building_number   = String()
    var apartment_number  = String()
    var latitude          = String()
    var longitude         = String()
    var isPrimary         = Bool()
    var trip_id           = String()
    var _id               = String()
    var activeFor: ZSActiveForObject? 
 

    override init() { }
    
    init(subPoint:JSON) {
        super.init()
        
        created_at            = subPoint["created_at"].stringValue
        updated_at            = subPoint["updated_at"].stringValue
        title                 = subPoint["title"].stringValue
        type                  = subPoint["type"].intValue
        province              = subPoint["province"].stringValue
        street_name           = subPoint["street_name"].stringValue
        building_number       = subPoint["building_number"].stringValue
        apartment_number      = subPoint["apartment_number"].stringValue
        latitude              = subPoint["latitude"].stringValue
        longitude             = subPoint["longitude"].stringValue
        isPrimary             = subPoint["isPrimary"].boolValue
        trip_id               = subPoint["trip_id"].stringValue
        _id                   = subPoint["_id"].stringValue
//        activeFor             =  ZSActiveForObject(activeFor: subPoint["activeFor"])

        if subPoint["activeFor"] != nil {
            activeFor =  ZSActiveForObject(activeFor: subPoint["activeFor"])
        }
        
//        if subPoint["activeFor"].arrayValue.count > 0 {
//            print(subPoint["activeFor"].arrayValue.count)
//            activeFor = subPoint["activeFor"].object as! ZSActiveForObject
//        }
    }
}
