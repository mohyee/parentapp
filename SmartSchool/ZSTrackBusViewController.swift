//
//  ZSTrackBusViewController.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/11/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import CoreLocation


var xoAssociationKey0: UInt8 = 0


class ZSTrackBusViewController: UIViewController, UIPickerViewDelegate,UIPickerViewDataSource, CLLocationManagerDelegate, GMSMapViewDelegate {

 
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var selectStudentButton: UIButton!
    
    @IBOutlet weak var pickerHolderView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var studentsData:  [ZSStudentObject]?  = nil
    var pointsData:    [ZSSubPointObject]? = [ZSSubPointObject]()
    var studentTrips:  [ZSTripObject]?     = [ZSTripObject]()
    var pinPoints :[ZSSubPointObject]?       = [ZSSubPointObject]()
    
    var selectedStudent = ZSStudentObject()
    var selectedStudentIndex = Int()
    var isPickerViewOpened: Bool = false
    var userLocation: CLLocation?
    var manager = CLLocationManager()
    
    var pointIds        = [String]()
    var subPointIds     = [String]()
    var busIds          = [String]()
    var journeyMarkers = [GMSMarker]()
    
    
    //MARK:- view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.manager = CLLocationManager()
        self.manager.delegate = self
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.manager.requestAlwaysAuthorization()
        self.manager.startUpdatingLocation()
        
        self.mapView.isMyLocationEnabled       = false
        self.mapView.isUserInteractionEnabled  = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self
        
        self.title = "متابعة الباص"
        
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.fetchChildrenTripsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketManager.shared.delegate = self
        SocketManager.shared.openConnection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.manager.stopUpdatingLocation()
        SocketManager.shared.closeConnection()
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        let location:CLLocation = locations[0]
//        
//        let latitude:CLLocationDegrees  = location.coordinate.latitude
//        let longitude:CLLocationDegrees = location.coordinate.longitude
//        
//        // The myLocation attribute of the mapView may be null
//        if userLocation == nil {
//            userLocation = mapView.myLocation
//            print("User's location: \(userLocation)")
//            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15)
//            self.mapView.camera = camera
//        } else {
//          //  print("User's location is unknown")
//        }
//    }
    
    //MARK:- Web services
    func fetchChildrenTripsFromServer() {
        
        let token: String    = UserDefaults.standard.string(forKey: "token")!
        let userID: String   = UserDefaults.standard.string(forKey: "userID")!
        let parentID: String = UserDefaults.standard.string(forKey: "parentID")!
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]
        
        let params = [Constants.PARAM_PARENT_ID: parentID,
                      Constants.PARAM_USER_ID: userID]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_GET_CHILDREN_TRIPS, parameters: params, headers: headers) { (data) in
            if data != nil {
                
                self.studentsData = [ZSStudentObject]()
                self.loading.stopAnimating()
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData["success"].boolValue)
                    for temp in jsonData["students"].arrayValue {
                        let studentObject = ZSStudentObject(student: temp)
                        self.studentsData?.append(studentObject)
                    }
                    
                    let studentAll = ZSStudentObject()
                    studentAll.text = "الجميع"
                    studentAll._id  = "-1000"
                    
                    self.studentsData?.append(studentAll)
                    self.studentsData?.reverse()
                    
                    self.selectedStudent = self.studentsData![self.selectedStudentIndex]
                    self.selectStudentButton.setTitle("\(self.selectedStudent.text)", for: UIControlState.normal)
                    
                    self.fetchActiveTripsFromServer()
                    //                    self.selectStudentButton.setTitle("\(self.selectedStudent.text)", for: UIControlState.normal)
                    //                    self.reloadSelectedStudentData()
                    
                } else {
                    self.loading.stopAnimating()
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                    })
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
    }
    
    
    func fetchActiveTripsFromServer() {
        
        let token: String    = UserDefaults.standard.string(forKey: "token")!
        let userID: String   = UserDefaults.standard.string(forKey: "userID")!
        let parentID: String = UserDefaults.standard.string(forKey: "parentID")!
        
        let params = ["parent_id": parentID,
                      "user_id": userID]
        
        let headers = [
            Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
            Constants.PARAM_ACCESS_TOKEN: token]
        
        self.loading.isHidden = false
        self.loading.color = UIColor.red
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: Constants.URL_GET_ACTIVE_TRIPS, parameters: params, headers: headers) { (data) in
            if data != nil {
                
                self.loading.stopAnimating()
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData["success"].boolValue)
                    
                    for temp in jsonData["trips"].arrayValue {
                        let tripObject = ZSTripObject(trip: temp)
                        
                        self.busIds.append(tripObject.bus_id[0])
                        //                        self.busIds = tripObject.bus_id
                        self.pointIds    = tripObject.point_ids
                        self.subPointIds = tripObject.subPoint_ids
                        self.studentTrips?.append(tripObject)
                        
                        print("Trips INFO :\n T rip Title : \(tripObject.title)\n Trip Text : \(tripObject.text)\n Trip SubPoints  :  \(self.subPointIds)\n Trip Bus Ids  :  \(self.busIds)\n Trip Points  :  \(self.pointIds)")
                    }
                    print("TRIPS COUNT : \(self.studentTrips?.count)")
                    
                    self.checkActiveTrips()
                    
                } else {
                    self.loading.stopAnimating()
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                    })
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
    }
    
    func joinRoom(forBus busID:String)  {
        
        let data  =  "bus-"+"\(busID)"
        
        SocketManager.shared.emit(messageID: "join Room For bus: "+"\(busID)", dataType: .text, eventKey: .joinRoom, data: data)
    }
    
    
    // MARK: Select Student Methods and Delegates
    
    @IBAction func selectStudentAction(_ sender: UIButton) {
        print("Select Student Action")
        
        self.pickerView.delegate   = self
        self.pickerView.dataSource = self
        
        if self.isPickerViewOpened {
            self.isPickerViewOpened = false
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = -108
            })
        } else {
            self.isPickerViewOpened = true
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = 64
            })
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studentsData?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(studentsData![row].text)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedStudentIndex = row
        self.selectStudentButton.setTitle("\(studentsData![row].text)", for: UIControlState.normal)
        
        if row == 0 {
            showAllMarkers()
        }
        
        else{
            if let busId = busIds[safe:row-1]{
                showMarker(forBus: busId)
            }
          }
    }
    
    func checkActiveTrips() {
        var isPrimary = false
        for trip in self.studentTrips! {
            for subIds in trip.subPoint_ids {
                for students in self.studentsData! {
                    if students.generalPoint == nil || students.generalPoint?.points == nil {
                    continue
                    }
                    let studentPoints = students.generalPoint!.points
                    for studentSubIds in studentPoints! {
                        if studentSubIds._id == subIds {
                            
                            if studentSubIds.isPrimary {
                                self.pinPoints?.append(studentSubIds)
                                isPrimary = true
                                print("Pin Title : \(studentSubIds.title)")
                            }
                            
                            if self.isSubPointActive(activeForObj: studentSubIds.activeFor) {
                                if isPrimary {
                                    let index = self.pointIds.endIndex - 1
                                    self.pinPoints?[index] = studentSubIds
                                    isPrimary = false
                                } else {
                                    self.pinPoints?.append(studentSubIds)
                                }
                                print("Pin Title : \(studentSubIds.title)")
                            }
                        }
                    }
                }
            }
        }
        
        if self.busIds.count > 0 {
            self.drawPinsOnMaps(tappedLocation: nil)
            JoinRoomForAllTrips()
            
        } else {
            Util.showInfoAlert(msg: "لا يوجد رحلات مفعلة", controller: self, callback: { (success) in
//                self.navigationController?.popViewController(animated: true)
                _ = self.navigationController?.popViewController(animated: true)

            })
        }
        
    }
    
    func JoinRoomForAllTrips () {
        for busId in busIds {
            joinRoom(forBus: busId)
        }
    }
    
    
    func drawPinsOnMaps(tappedLocation: CLLocation?) {
        self.mapView.clear()
        let path = GMSMutablePath()
        
        for location in self.pinPoints! {
            let pinIcon = UIImage(named: "storesMapNeasrestPin.pin")
            
            if tappedLocation != nil {
                if String(tappedLocation!.coordinate.longitude) == location.longitude && String(tappedLocation!.coordinate.latitude) == location.latitude {
                    
                }
            }
            path.add(CLLocationCoordinate2DMake(Double(location.latitude)!, Double(location.longitude)!))
            
            let pinLocation = CLLocationCoordinate2DMake(Double(location.latitude)!, Double(location.longitude)!)
            
            let marker   = GMSMarker(position: pinLocation)
            marker.icon  = pinIcon
            marker.map = self.mapView
        }
        
        let bounds = GMSCoordinateBounds(path: path)
        self.mapView!.animate(toZoom: 15)
        self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //** show bus number for bus's markers
        guard marker.busID == nil else{
            return false
        }
    
        let tappedLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        
        for location in self.pinPoints! {
            if tappedLocation.coordinate.longitude == Double(location.longitude) && tappedLocation.coordinate.latitude == Double(location.latitude) {
                print("Tapped Location : \(tappedLocation)")
                print("Location Name : \(location.title)")
                marker.title = location.title
                self.mapView.selectedMarker = marker
            }
        }
        return true
    }
    
    func isSubPointActive(activeForObj: ZSActiveForObject?)-> Bool {
        
        var isActiveFor = false
        
        if (activeForObj == nil || activeForObj?.start == 0 || activeForObj?.end == 0 ) {
            return false
        }
        
        let currentDate = Date()
        let convertedDate = currentDate.timeIntervalSince1970
        let currentDateInMillis =  Int64(convertedDate*1000)
        
        let startDate = (activeForObj?.start)!
        let endDate   = (activeForObj?.end)!
        
        if (currentDateInMillis >= startDate && currentDateInMillis <= endDate) {
            isActiveFor = true
        }
        return isActiveFor
    }
    
    //    public isSubPointActive(activeForObj: ZSActiveForObject)->bool {
    //    boolean isActiveFor = false;
    //
    //    if (activeFor == null || activeFor.getStart().length() == 0
    //    || activeFor.getEnd().length() == 0)
    //    return false;
    //
    //    Date nowDate = new Date();
    //    long start = Utilities.castStringToLong(activeFor.getStart());
    //    long end = Utilities.castStringToLong(activeFor.getEnd());
    //    long now = nowDate.getTime();
    //
    //    if (now >= start && now <= end) {
    //    isActiveFor = true;
    //    }
    //    return isActiveFor;
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateMarkersOnMap(forJourny journy : ZSJournyObject) {
        
        guard journeyMarkers.count > 0 else{
            addMarker(forJourny: journy)
            return
        }
        
        let journyMarkerStatus = getJournyMarkerStatus(forJourny:journy)
        
        //** mave marker if its added before
        if journyMarkerStatus.isAddedBefor{
        
            moveMarker(journyMarkerStatus.marker!,to:journy.location)
        }
           //** add marker for first time
        else{
            addMarker(forJourny:journy)
        }
    }
    
    func getJournyMarkerStatus(forJourny journy:ZSJournyObject) -> (isAddedBefor:Bool,marker:GMSMarker?)  {
        
        for marker in journeyMarkers{
            if journy.busId == marker.busID{
                return (true,marker)
            }
        }
        
        return (false,nil)
    }
    
    
    func addMarker(forJourny journy:ZSJournyObject) {
        
        if journy.location != nil {
            let marker =  GMSMarker()
            marker.busID = journy.busId
            marker.position = journy.location!
            marker.icon = #imageLiteral(resourceName: "busIcon")
            marker.title = journy.busNumber
            marker.map = mapView
            journeyMarkers.append(marker)
        }
        
    }
    
    func moveMarker(_ marker:GMSMarker, to newPosition: CLLocationCoordinate2D?) {
        if newPosition != nil {
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        marker.position = newPosition!
        CATransaction.commit()
            
 
        }}
    
    func showMarker(forBus busId:String){
        for marker in journeyMarkers{
            if busId == marker.busID{
                marker.map = mapView
                let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: 15)
                self.mapView.camera = camera

            }
                //** hide  other markers
            else{
                marker.map = nil
            }
        }
    }

    func showAllMarkers() {
        for marker in journeyMarkers{
                marker.map = mapView
            }
    }
}

//MARK:- Socket Manager Delegate methods
extension ZSTrackBusViewController:SocketManagerDelegate{
    
    func socketManagerDidConnect() {
        print("socketManagerDidConnect")
    }
    
    func socketManagerDidDisconnect() {
        print("socketManagerDidDisconnect")
    }
    
    func socketManagerDidReceiveError(error: SocketManagerError) {
        print("socketManagerDidReceiveError" + "\(error)")
    }
    
    func socketManagerDidReceive(error: SocketManagerError, forEvent event: EventsKeys) {
        print("socketManagerDidReceiveError" + "\(error)"+" For Event"+" "+"\(event)")
    }
    
    func socketManagerDidReceiveMessageID(messageId: String, data: AnyObject?) {
        
    }
    
    func socketManagerDidReceiveData(data: JSON, messageDataType: MessageDataType, forEvent event: EventsTypes, messageID: String) {
        if event == EventsTypes.busLocation{
            let journy = ZSJournyObject(data)
            updateMarkersOnMap(forJourny: journy)
        }
    }
}


extension GMSMarker{
    var busID:String?{
        get{
            return objc_getAssociatedObject(self, &xoAssociationKey0) as? String
        }
        set (newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey0, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}


extension Collection where Indices.Iterator.Element == Index {
    
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Generator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}


