//
//  ZSActiveForObject.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/24/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSActiveForObject: NSObject {

    var start: Int64!  = Int64()
    var end:   Int64!  = Int64()
    
    override init() { }
    
    init(activeFor:JSON) {
        super.init()
        
        start    = activeFor["start"].int64Value
        end      = activeFor["end"].int64Value
    }
    
}
