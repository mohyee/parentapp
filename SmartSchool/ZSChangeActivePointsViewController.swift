//
//  ZSChangeActivePointsViewController.swift
//  SmartSchool
//
//  Created by Mo7yee on 2/25/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ZSChangeActivePointsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    var selectedRow = [String:NSIndexPath]()
    var studentsData:  [ZSStudentObject]?  = [ZSStudentObject]()
    var pointsData:    [ZSSubPointObject]? = [ZSSubPointObject]()
    
    var selectedStudent = ZSStudentObject()
    var selectedPointID = String()
    
    var isPickerViewOpened: Bool = false
    var selectedStudentIndex = Int()
    
    var primaryCell = UITableViewCell()
    
    var studentTrips:     [ZSTripObject]?       = [ZSTripObject]()
    var dropTrips:        [ZSSubPointObject]?   = [ZSSubPointObject]() // type 1 pull مغادرة
    var pickupTrips:      [ZSSubPointObject]?   = [ZSSubPointObject]() // type 2 drop حضور
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var selectStudentButton: UIButton!
    @IBOutlet weak var pickerHolderView: UIView!
    @IBOutlet weak var pickerView:       UIPickerView!
    @IBOutlet weak var pickerButtonView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "تغيير النقطة المفعلة"
        
        self.navigationController?.navigationBar.topItem!.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.fetchTripsFromServer()
        self.tableView.isHidden = true
    }
    
    func fetchTripsFromServer() {
        
        let token: String    = UserDefaults.standard.string(forKey: "token")!
        let userID: String   = UserDefaults.standard.string(forKey: "userID")!
        let parentID: String = UserDefaults.standard.string(forKey: "parentID")!
        
        let urlString = Constants.URL_GET_CHILDREN_TRIPS
        
        let headers = [ Constants.PARAM_CONTENT_TYPE: Constants.CONTENT_TYPE,
                        Constants.PARAM_ACCESS_TOKEN: token ]
        
        let params = [Constants.PARAM_PARENT_ID: parentID,
                      Constants.PARAM_USER_ID: userID]
        
        self.loading.isHidden = false
        self.loading.startAnimating()
        
        MITWSConectionManager.shared.postJSONResponse(url: urlString, parameters: params, headers: headers) { (data) in
            if data != nil {
                self.loading.stopAnimating()
                self.tableView.isHidden = false
                
                let jsonData = JSON(data!)
                if jsonData["success"].boolValue {
                    print(jsonData["success"].boolValue)
                    for temp in jsonData["students"].arrayValue {
                        let studentObject = ZSStudentObject(student: temp)
                        self.studentsData?.append(studentObject)
                    }
                    
                    self.selectedStudent = self.studentsData![self.selectedStudentIndex]
                    self.selectedPointID = self.selectedStudent.generalPoint!._id
                    self.selectStudentButton.setTitle("\(self.selectedStudent.text)", for: UIControlState.normal)
                    self.reloadSelectedStudentData()
                    
                } else {
                    self.loading.stopAnimating()
                    Util.showInfoAlert(msg: jsonData["message"].stringValue, controller: self, callback: { (success) in
                    })
                    //                    KVNProgress.showError(withStatus: jsonData["message"].stringValue)
                }
            } else {
                self.loading.stopAnimating()
                print("Connection Error")
            }
        }
    }
    
    func addSelectedCellWithSection(indexPath:NSIndexPath) ->NSIndexPath?
    {
        let existingIndexPath = selectedRow["\(indexPath.section)"]
        if existingIndexPath == nil {
            selectedRow["\(indexPath.section)"]=indexPath
        }else
        {
            selectedRow["\(indexPath.section)"]=indexPath
            return existingIndexPath
        }
        return nil;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0{
            return "حضور"
        } else {
            return "مغادرة"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return self.pickupTrips!.count
        } else {
            return self.dropTrips!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var isPrimaryActive = true
        
        if indexPath.section == 0 { //Pickup Section - Type 2 drop حضور
            let subPointObject = (self.pickupTrips?[indexPath.row])! as ZSSubPointObject
            cell.textLabel?.text = subPointObject.title
            
            if self.isSubPointActive(activeForObj: subPointObject.activeFor) {
                
                cell.textLabel?.textColor = UIColor.green
                self.primaryCell.textLabel?.textColor = UIColor.black
                isPrimaryActive = false
                
            } else {
                cell.textLabel?.textColor = UIColor.black
                isPrimaryActive = true
            }
            
            if subPointObject.isPrimary {
                cell.textLabel?.text = "* " + subPointObject.title
                primaryCell = cell
                if isPrimaryActive {
                    cell.textLabel?.textColor = UIColor.green
                }
            } else {
                cell.textLabel?.text = subPointObject.title
            }
        }
        
        if indexPath.section == 1 {
            
            let subPointObject = (self.dropTrips?[indexPath.row])! as ZSSubPointObject
            cell.textLabel?.text = subPointObject.title
            
            if self.isSubPointActive(activeForObj: subPointObject.activeFor) {
                cell.textLabel?.textColor = UIColor.green
                self.primaryCell.textLabel?.textColor = UIColor.black
                isPrimaryActive = false
            }
            else {
                cell.textLabel?.textColor = UIColor.black
                isPrimaryActive = true
            }
            
            if subPointObject.isPrimary {
                cell.textLabel?.text = "* " + subPointObject.title
                primaryCell = cell
                if isPrimaryActive {
                    cell.textLabel?.textColor = UIColor.green
                }
            } else {
                cell.textLabel?.text = subPointObject.title
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 0 {
            let subPointObject = (self.pickupTrips?[indexPath.row])! as ZSSubPointObject
            self.showAlert(subPointObject: subPointObject)
        } else if indexPath.section == 1 {
            let subPointObject = (self.dropTrips?[indexPath.row])! as ZSSubPointObject
            self.showAlert(subPointObject: subPointObject)
        }
        tableView.deselectRow(at: indexPath, animated: false)
        
        //        let cell = self.tableView.cellForRow(at: indexPath)
        //        let previusSelectedCellIndexPath = self.addSelectedCellWithSection(indexPath: indexPath as NSIndexPath)
        //
        //        if previusSelectedCellIndexPath != nil {
        //            let previusSelectedCell = self.tableView.cellForRow(at: previusSelectedCellIndexPath! as IndexPath)
        //
        //            previusSelectedCell?.accessoryType = UITableViewCellAccessoryType.none
        //            cell?.accessoryType = .checkmark
        //
        //            if indexPath.section == 0 {
        //                let subPointObject = (self.pickupTrips?[indexPath.row])! as ZSSubPointObject
        //                self.showAlert(subPointObject: subPointObject)
        //            } else if indexPath.section == 1 {
        //                let subPointObject = (self.dropTrips?[indexPath.row])! as ZSSubPointObject
        //                self.showAlert(subPointObject: subPointObject)
        //            }
        //            tableView.deselectRow(at: previusSelectedCellIndexPath! as IndexPath, animated: true);
        //        }
        //        else
        //        {
        //            cell?.accessoryType = .checkmark
        //        }
        
    }
    
    func isActiveForPoint(_ activePointObject: ZSActiveForObject) -> Bool {
        
        if activePointObject.start != 0 && activePointObject.end != 0{
            
            let nowTime = self.currentTimeMillis()
            let start   = Int64(activePointObject.start!)
            let end     = Int64(activePointObject.end!)
            
            if nowTime >= start && nowTime <= end {
                return true
            }
            print(activePointObject.start)
        }
        return false
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    func showAlert(subPointObject: ZSSubPointObject) {
        let alert = UIAlertController(title: nil, message: "ماذا تريد ان تفعل ؟", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "تغيير وقت النقطة", style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: Constants.SEGUE_CALENDAR, sender: subPointObject)
            print("User click Approve button")
        }))
        
        alert.addAction(UIAlertAction(title: "عرض النقطة على الخريطة", style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: Constants.SEGUE_MAP, sender: subPointObject)
            print("User click Edit button")
        }))
        
        alert.addAction(UIAlertAction(title: "إلغاء", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func reloadSelectedStudentData() {
        
        self.selectedStudent = self.studentsData![self.selectedStudentIndex]
        let selectedGeneralPoint  = selectedStudent.generalPoint
        let subPoints = selectedGeneralPoint!.points! as [ZSSubPointObject]
        self.dropTrips   = [ZSSubPointObject]()
        self.pickupTrips = [ZSSubPointObject]()
        
        for i in 0..<subPoints.count {
            
            let subPointObject = subPoints[i] as ZSSubPointObject
            if subPointObject.type == 1 {         //1 Drop
                self.dropTrips?.append(subPointObject)
            } else if subPointObject.type == 2 {  //2 Pickup
                self.pickupTrips?.append(subPointObject)
            } else if subPointObject.type == 3 {  //3 Pickup & Drop
                self.dropTrips?.append(subPointObject)
                self.pickupTrips?.append(subPointObject)
            }
        }
        self.tableView.reloadData()
    }
    
    // MARK: Select Student Methods and Delegates
    
    @IBAction func selectStudentAction(_ sender: UIButton) {
        print("Select Student Action")
        
        self.pickerView.delegate   = self
        self.pickerView.dataSource = self
        
        if self.isPickerViewOpened {
            self.isPickerViewOpened = false
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = -108
            })
        } else {
            self.isPickerViewOpened = true
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerHolderView.frame.origin.y = 64
            })
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studentsData!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(studentsData![row].text)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedStudentIndex = row
        self.selectStudentButton.setTitle("\(studentsData![row].text)", for: UIControlState.normal)
        self.reloadSelectedStudentData()
    }
    
    func isSubPointActive(activeForObj: ZSActiveForObject?)-> Bool {
        
        var isActiveFor = false
        
        if (activeForObj == nil || activeForObj?.start == 0 || activeForObj?.end == 0 ) {
            return false
        }
        
        let currentDate = Date()
        let convertedDate = currentDate.timeIntervalSince1970
        let currentDateInMillis =  Int64(convertedDate*1000)
        
        let startDate = (activeForObj?.start)!
        let endDate   = (activeForObj?.end)!
        
        if (currentDateInMillis >= startDate && currentDateInMillis <= endDate) {
            isActiveFor = true
        }
        return isActiveFor
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let subPointObject = sender as! ZSSubPointObject
        
        if segue.identifier == Constants.SEGUE_MAP {
            let mapViewController = segue.destination as! ZSMapViewController
            mapViewController.selectedSubPoint = subPointObject
        } else if segue.identifier == Constants.SEGUE_CALENDAR {
            let calendarViewController = segue.destination as! ZSCalendarViewController
            calendarViewController.selectedSubPoint = subPointObject
            calendarViewController.selectedPointID  = selectedPointID
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
