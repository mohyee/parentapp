//
//  ZSStudentObject.swift
//  SmartSchool
//
//  Created by Mohyee Tamimi on 2/10/17.
//  Copyright © 2017 Mohyee Tamimi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZSStudentObject: NSObject {
    
    var _id                    = String()
    var created_at             = String()
    var updated_at             = String()
    var first_name             = String()
    var father_name            = String()
    var grand_father_name      = String()
    var last_name              = String()
    var national_id            = String()
    var __v                    = Int() //Version
    var educational_documents  = [String]()
    var classroom_id           = [String]()
    var school_id              = [String]()
    var roles                  = [String]()
    var parent_ids             = [String]()
    var personal_documents     = [String]()
    var text                   = String()
    var id                     = String()
    var trips: [ZSTripObject]? = [ZSTripObject]()
    var generalPoint: ZSGeneralPointObject?
    
//    var generalPointObject = temp["point"].dictionaryValue
//    var subPoints: [ZSSubPointObject]?  = [ZSSubPointObject]()
    
    override init() { }
    
    init(student:JSON) {
        super.init()
        
        _id                   = student["_id"].stringValue
        created_at            = student["created_at"].stringValue
        updated_at            = student["updated_at"].stringValue
        first_name            = student["first_name"].stringValue
        father_name           = student["father_name"].stringValue
        grand_father_name     = student["grand_father_name"].stringValue
        last_name             = student["last_name"].stringValue
        national_id           = student["national_id"].stringValue
        __v                   = student["__v"].intValue
        educational_documents = student["educational_documents"].arrayObject as! [String]
        classroom_id          = student["classroom_id"].arrayObject as! [String]
        school_id             = student["school_id"].arrayObject as! [String]
        roles                 = student["roles"].arrayObject as! [String]
        parent_ids            = student["parent_ids"].arrayObject as! [String]
        personal_documents    = student["personal_documents"].arrayObject as! [String]
        text                  = student["text"].stringValue
        id                    = student["id"].stringValue
        generalPoint          =  ZSGeneralPointObject(generalPoint: student["point"])
        
        for temp in student["trips"].arrayValue {
            let tripObject = ZSTripObject(trip: temp)
            trips?.append(tripObject)
        }
    }
}

//    "students": [
//    {
//    "_id": "5872181908db91dc77e896c9",
//    "created_at": "Sun Jan 08 2017 10:44:41 GMT+0000 (UTC)",
//    "updated_at": "Wed Feb 01 2017 12:26:26 GMT+0000 (UTC)",
//    "first_name": "haya",
//    "father_name": "salm ",
//    "grand_father_name": "sari",
//    "last_name": "yaser",
//    "national_id": "",
//    "__v": 0,
//    "educational_documents": [],
//    "classroom_id": [
//    "587214e008db91dc77e89635"
//    ],
//    "school_id": [
//    "587153e429a577d62e2cae2f"
//    ],
//    "roles": [],
//    "parent_ids": [
//    "5872181908db91dc77e896c7"
//    ],
//    "personal_documents": [],
//    "text": "haya salm  sari yaser",
//    "id": "5872181908db91dc77e896c9"
//    }
